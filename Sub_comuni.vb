﻿Imports System.IO
Module Sub_comuni

    Public Structure stat
        Public Host As String
    End Structure

    Public Structure pia
        Public Nome_operatore As String
        Public Matricola_Piastra As String

    End Structure
    Public Stringa_Salvataggio As String
    Public Piastra As pia
    Public Status As stat
    Public Traspric As String
    Public Direct As Boolean = True
    Public Salva_com As Boolean
    Public Stringa_di_connessione As String
    Public Stringa_Errore As String
    Public Debug_test As Boolean
    Public Attesa_Tasto As Boolean
    Public Debug_pausa As Integer
    Public WorkDir As String
    Public Salta_DWL As Boolean
    Public Salta_trasf_fw As Boolean
    Public Stringa_Salvataggio_Pattern_Preset As String = ""
    Public No_Timeout As Boolean = False
    Public Tasto As Integer
    Public Cod_piastra As String
    Public Matr_attrezzatura As String
    Public k_timeout As Integer
    Public Stringa_vuota As Boolean
    Public Errore_FW As String
    Public Ripeti_trasp As Boolean
    Public UVSoloCanaleSx As Boolean

    Public Function stringa_invio(ByVal stringa As String) As String
        stringa_invio = ""
        For a = 1 To Len(stringa) Step 2

            stringa_invio = stringa_invio + Chr("&h" + (Mid$(stringa, a, 2)))
        Next

    End Function
    Public Function stringa_ricezione(ByVal stringa As String) As String
        Dim aaa As String
        stringa_ricezione = ""
        If stringa <> "" Then
            For a = 1 To Len(stringa)
                aaa = Hex$(Asc(Mid$(stringa, a, 1)))
                If Len(aaa) = 1 Then aaa = "0" + aaa
                stringa_ricezione = stringa_ricezione + aaa
            Next
        End If

    End Function
   


    Public Sub MySleep(ByVal Millisec As Integer)



        If (Millisec = 5) Then
            Millisec = 8
        End If

        If Millisec > 500 Then
            Dim aa As Integer
            aa = Millisec / 10
            For a = 0 To aa
                System.Threading.Thread.Sleep(10)
                Application.DoEvents()
            Next
        Else
            System.Threading.Thread.Sleep(Millisec)
        End If

    End Sub

    Public Function METTE_0(ByVal pippo As String)
        Dim stringa As String
        stringa = ""

        For a = 1 To Len(pippo)

            stringa = stringa + "0" + Mid$(pippo, a, 1)

        Next

        METTE_0 = stringa

    End Function

    Function dec_hex(ByVal a) As String

        If a.ToString <> "" Then

            dec_hex = Hex(a) : If Len(dec_hex) = 1 Then dec_hex = "0" + dec_hex
            dec_hex = dec_hex
        Else
            dec_hex = ""

        End If

    End Function

    Function TOGLIE_0(ByVal pippo As String)
        Dim stringa As String = ""
        For a = 2 To Len(pippo) Step 2

            stringa = stringa + Mid$(pippo, a, 1)

        Next

        TOGLIE_0 = stringa

    End Function


    Public Function hex_bin(ByVal hex)
        Dim b As String
        hex_bin = ""
        For a = 1 To Len(hex)

            b = dec_bin("&h" + Mid$(hex, a, 1))
            b = Mid(b, 5)

            hex_bin = hex_bin + b
        Next



    End Function

    Public Function bin_dec(ByVal bin)
        Dim aa As Long = 0
        For a = 0 To Len(bin) - 1
            Application.DoEvents()
            aa = aa + Val(Mid$(bin, Len(bin) - a, 1) * 2 ^ a)
        Next

        bin_dec = aa

    End Function
    Public Function dec_bin(ByVal dec)
        Dim aa As Integer
        On Error Resume Next
        dec_bin = ""
        Do While Not dec = 0
            aa = dec \ 2

            If aa * 2 = Val(dec) Then
                dec_bin = "0" + dec_bin
            Else
                dec_bin = "1" + dec_bin
            End If
            dec = Int(dec / 2)
        Loop

        If dec_bin = "" Then dec_bin = "0"

        For a = 1 To 8 - Len(dec_bin)

            dec_bin = "0" + Trim(dec_bin)
        Next

    End Function

    Public Function int_offset(ByVal st As String) As Integer

        If Mid$(st, 2, 1) = "0" Then
            int_offset = -(Val("&h" + Mid$(st, 3, 2)))
        Else
            int_offset = (Val("&h" + Mid$(st, 3, 2)))
        End If

    End Function

    Public Function bin_hex(ByVal bin)
        Dim p
        bin_hex = ""
        If bin = "0" Then bin_hex = "0" : Exit Function
        Do While Not Len(bin) Mod 4 = 0
            bin = "0" + bin
            Application.DoEvents()
        Loop

        For a = 1 To Len(bin) Step 4
            Application.DoEvents()
            p = Hex(bin_dec(Mid$(bin, a, 4)))
            bin_hex = Trim((bin_hex + p))
        Next

    End Function
    Public Sub Attendi_Invio()
        Principale.Focus()
        Tasto = 0
        Do While Not Tasto = 13

            Application.DoEvents()
        Loop


    End Sub

    Sub Attendi_Tasto()
        Principale.Focus()
        Tasto = 0
        Do While Not Tasto <> 0
            Application.DoEvents()
        Loop

    End Sub
End Module

