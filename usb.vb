﻿Imports System.Runtime.InteropServices
Imports System.Text



'/**//*******************************************************************************************************************
' * @file oemr_usb.h
' * Definizioni per il modulo principale DLL per colloquio USB con lettori NG.
' *
' * @author Maurizio Nervo
' * @version $Id: oemr_usb.h 2 2013-04-12 11:58:34Z mnervo $
' * @revision 2.0.0
' * @copyright (C) 2005-2013 CTS cashpro S.p.A.
' **********************************************************************************************************************/
'#ifndef OEMR_USB_H
'#define OEMR_USB_H

'/***********************************************************************************************************************
' * Costanti generiche
' **********************************************************************************************************************/
'#define OEMR_TYPE_SEF				0				/**< Costante per modello SEF */
'#define OEMR_TYPE_LEF				1				/**< Costante per modello LEF */
'#define OEMR_SPT_SIZE				2048			/**< Dimensione blocco dati SPT */
'#define OEMR_PAGE_SIZE				(256 * 256)		/**< Dimensione pagina di memoria (immagini CIS) */
'#define OEMR_UV_SIZE				128				/**< Dimensione blocco dati vettore UV in bytes */
'#define OEMR_TAPE_STREAM_SIZE		1024			/**< Dimensione dello stream USB del Tape Sensor in bytes */
'#define OEMR_TAPE_IMG_SIZE			(8 * 100)		/**< Dimensione immagine Tape Sensor in bytes */
'#define OEMR_MAG_IMG_SIZE			(9 * 200)		/**< Dimensione immagine Mag Sensor in bytes */
'#define OEMR_MAG_DIAG_SIZE			(9 * 2048)		/**< Dimensione immagine Mag diagnostica in bytes */
'#define OEMR_STAT_SIZE				0x5000			/**< Dimensione blocco dati stiatistiche di sessione */
'#define OEMR_USB_TIMEOUT			1000			/**< Timeout di I/O su USB in ms */
'#define OEMR_USB_TX_EP				1				/**< Endpoint USB per trasmissione */


'#define LUSB_TX_EP					0x02			/**< Endpoint LibUSB per trasmissione*/
'#define LUSB_RX_STD_EP				0x81			/**< Endpoint LibUSB per ricezione standard */
'#define LUSB_RX_DMA_EP				0x83			/**< Endpoint LibUSB per ricezione DMA */

'/***********************************************************************************************************************
' * Struttura di stato delle operazioni di I/O su USB
' **********************************************************************************************************************/


'// Costanti dei codici risultato operazioni di I/O
'#define USB_IO_RES_OK				0				/**< I/O terminato correttamente */
'#define USB_IO_RES_ERR				-1				/**< I/O fallito per errore */
'#define USB_IO_RES_TOUT				-2				/**< I/O fallito per timeout */
'#define USB_IO_RES_MORE				-3				/**< I/O interrotto senza aver letto tutti i dati */

'/***********************************************************************************************************************
' * Comandi USB e parametri
' **********************************************************************************************************************/
'// ID dei comandi USB
'#define OEMR_USB_READ_PAGE			0x30			/**< Lettura pagina di memoria video */
'	#define OEMR_PAGE_MAX				0x1F			/**< Massimo n° di pagina disponibile */
'	#define OEMR_PAGE_GR_FRONT			0x00			/**< Pagina dell'immagine Green Front */
'	#define OEMR_PAGE_IR_FRONT			0x01			/**< Pagina dell'immagine InfraRed Front */
'	#define OEMR_PAGE_RD_FRONT			0x02			/**< Pagina dell'immagine Red Front */
'	#define OEMR_PAGE_GR_REAR			0x04			/**< Pagina dell'immagine Green Rear */
'	#define OEMR_PAGE_IR_REAR			0x05			/**< Pagina dell'immagine InfraRed Rear */
'	#define OEMR_PAGE_RD_REAR			0x06			/**< Pagina dell'immagine Red Rear */
'	#define OEMR_PAGE_BUFFER_1			0x08			/**< Indice da sommare al n° di pagina per il buffer 1 */
'#define OEMR_USB_GET_PATTERN		0x43			/**< Lettura pattern */
'	#define OEMR_PATTERN_SPT			0x0A			/**< Lettura immagine SPT */
'	#define OEMR_PATTERN_UV				0x04			/**< Lettura pattern UV */
'	#define OEMR_PATTERN_TAPE_RAW		0x06			/**< Lettura pattern Tape Sensor Raw */
'	#define OEMR_PATTERN_TAPE_POST		0x86			/**< Lettura pattern Tape Sensor post-elaborato */
'	#define OEMR_PATTERN_TAPE_DIAG		0x12            /**< Lettura pattern Diagnostica ultrasuoni */
'	#define OEMR_PATTERN_MAG			0x07			/**< Lettura pattern Mag Sensor */
'	#define OEMR_PATTERN_MAG_DIAG		0x0B			/**< Lettura pattern Diagnostica magnetico */
'#define OEMR_USB_GET_STAT			0x42			/**< Lettura statistiche della sessione di deposito */

'***************************************************************************************************
'* Funzioni Public
'**********************************************************************************************************************/
'BOOL WINAPI OEMR_OpenUsbConnection(LPOEMR_USB_IO lpOUIO);
'VOID WINAPI OEMR_CloseUsbConnection(VOID);
'BOOL WINAPI OEMR_SendUsbCommand(LPCVOID buf, DWORD len, UINT timeout);
'BOOL WINAPI OEMR_ReadUsbDataStream(BYTE endpoint, LPVOID buf, DWORD len, UINT timeout);
'BOOL WINAPI OEMR_ReadUsbDataPage(BYTE page, LPVOID buf);
'BOOL WINAPI OEMR_ReadUsbPattern(BYTE pattern, BYTE bufNum, LPVOID buf);

'#endif /* OEMR_USB_H */

'typedef struct {
'	DWORD	dwBytes;				/**< N° di bytes da trasferire */
'	DWORD	dwDoneBytes;			/**< N° di bytes trasferiti correttamente */
'	INT		iResult;				/**< Codice risultato dell'operazione di I/O */
'} OEMR_USB_IO, *LPOEMR_USB_IO;


'#define OEMR_USB_RX_STD_EP			0				/**< Endpoint USB per ricezione standard */
'#define OEMR_USB_RX_DMA_EP			2				/**< Endpoint USB per ricezione DMA */


Module USB
    'BOOL WINAPI OEMR_OpenUsbConnection(LPOEMR_USB_IO lpOUIO);
    <DllImport("OEMR_USB.dll", SetlastError:=True, CallingConvention:=CallingConvention.Winapi)> Public Function OEMR_OpenUsbConnection(ByVal lpOUIO As Int32) As Boolean
    End Function
    'VOID WINAPI OEMR_CloseUsbConnection(VOID)
    <DllImport("OEMR_USB.dll", SetlastError:=True, CallingConvention:=CallingConvention.Winapi)> Public Sub OEMR_CloseUsbConnection()
    End Sub
    'BOOL WINAPI OEMR_SendUsbCommand(LPCVOID buf, DWORD len, UINT timeout);
    <DllImport("OEMR_USB.dll", SetlastError:=True, CallingConvention:=CallingConvention.Winapi)> Public Function OEMR_SendUsbCommand(ByVal buf As Byte(), ByVal len As Int32, ByVal timeout As UInt32) As Boolean
    End Function
    'BOOL WINAPI OEMR_ReadUsbDataStream(BYTE endpoint, LPVOID buf, DWORD len, UINT timeout);
    <DllImport("OEMR_USB.dll", SetlastError:=True, CallingConvention:=CallingConvention.Winapi)> Public Function OEMR_ReadUsbDataStream(ByVal DMA As Byte, ByVal buf As Byte(), ByVal len As Int32, ByVal timeout As UInt32) As Boolean
    End Function
    'BOOL WINAPI OEMR_ReadUsbDataPage(BYTE page, LPVOID buf);
    <DllImport("OEMR_USB.dll", SetlastError:=True, CallingConvention:=CallingConvention.Winapi)> Public Function OEMR_ReadUsbDataPage(ByVal Page As Byte, ByVal buf As Byte()) As Boolean
    End Function
    'BOOL WINAPI OEMR_ReadUsbPattern(BYTE pattern, BYTE bufNum, LPVOID buf);
    <DllImport("OEMR_USB.dll", SetlastError:=True, CallingConvention:=CallingConvention.Winapi)> Public Function OEMR_ReadUsbPattern(ByVal Pattern As Byte, ByVal BufNum As Byte, ByVal buf As Byte()) As Boolean
    End Function

    Function USBB(ByVal Comando As String, ByVal Nbyte_Ric As Integer, ByVal Tout As Integer, ByVal isDMA As Boolean) As Byte()

        Dim Buf_Ric(Nbyte_Ric - 1) As Byte

        Dim aa As Boolean
        Dim DMA As Byte
        If isDMA = True Then
            DMA = 2
        Else
            DMA = 0
        End If

        Dim USB_tx((Comando.Length / 2) - 1) As Byte

        Dim b As Integer = 0

        For a = 1 To Len(Comando) - 1 Step 2
            USB_tx(b) = Val("&h" & Mid$(Comando, a, 2))
            b = b + 1
        Next

        Dim len_tx As Int32 = USB_tx.Length


        aa = OEMR_OpenUsbConnection(0)

        aa = OEMR_SendUsbCommand(USB_tx, len_tx, 1000)

        Dim kk(10) As Integer
        Dim gg As Integer = 0

        'Dim Nbyte_Ric_new As Integer
        'Dim buf_ric_integer() As Byte
        'If Nbyte_Ric > 65536 Then

        '    For a = 0 To Nbyte_Ric - 1 Step 65536
        '        If Nbyte_Ric - a > 65536 Then
        '            ReDim buf_ric_integer(65535)
        '            Nbyte_Ric_new = 65536
        '        Else
        '            ReDim buf_ric_integer(Nbyte_Ric - a - 1)
        '            Nbyte_Ric_new = Nbyte_Ric - a
        '        End If
        '        gg = gg + 1
        '        kk(gg) = Nbyte_Ric_new

        '        aa = OEMR_ReadUsbDataStream(DMA, buf_ric_integer, Nbyte_Ric_new, Tout)
        '        Buffer.BlockCopy(buf_ric_integer, 0, Buf_Ric, a, Nbyte_Ric_new)
        '        If aa = False Then
        '            'Stop
        '        End If
        '    Next
        'Else
        aa = OEMR_ReadUsbDataStream(DMA, Buf_Ric, Nbyte_Ric, Tout)
        'End If

        'If no_timeout_USB = False Then
        If (No_Timeout = False) Then
            If aa = False Then
                Throw New IOTimeoutException("Read USB Timeout")
            End If
        End If

        OEMR_CloseUsbConnection()

        USBB = Buf_Ric

    End Function

    Function USBB(ByVal Comando As String, ByVal Nbyte_Ric As Int32, ByVal Tout As Int32, ByVal isDMA As Boolean, ByVal salta_open_close As Boolean) As Byte()

        Dim Buf_Ric(Nbyte_Ric - 1) As Byte
        Dim aa As Boolean
        Dim DMA As Byte
        If isDMA = True Then
            DMA = 2
        Else
            DMA = 0
        End If

        Dim USB_tx((Comando.Length / 2) - 1) As Byte

        Dim b As Integer = 0

        For a = 1 To Len(Comando) - 1 Step 2
            USB_tx(b) = Val("&h" & Mid$(Comando, a, 2))
            b = b + 1
        Next
        If salta_open_close = False Then
            aa = OEMR_OpenUsbConnection(0)
        End If
        aa = OEMR_SendUsbCommand(USB_tx, USB_tx.Length, 500)

        aa = OEMR_ReadUsbDataStream(DMA, Buf_Ric, Nbyte_Ric, Tout)

        If salta_open_close = False Then
            OEMR_CloseUsbConnection()
        End If

        USBB = Buf_Ric

    End Function

    Function Usb_Prova() As Boolean
        Dim aaa As String
        Dim aa() As Byte
        Dim comando As String = "20"
        aa = USBB(comando, 12, 500, False)
        aaa = stringa_invio(BitConverter.ToString(aa).Replace("-", String.Empty))
        If String.Compare(aaa, "123456prova") Then
            Usb_Prova = True
            Exit Function
        End If
        Usb_Prova = False
    End Function
End Module


