﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading
Imports System.ComponentModel
Imports System.IO


Public Class Principale
    Inherits System.Windows.Forms.Form



    '// Private members
    Private miComPort As Integer
    Friend WithEvents btnOpenCom As System.Windows.Forms.Button
    Friend WithEvents btnCloseCom As System.Windows.Forms.Button
    Friend WithEvents btnTx As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTx As System.Windows.Forms.TextBox
    Friend WithEvents txtRx As System.Windows.Forms.TextBox
    Friend WithEvents btnRx As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtBytes2Read As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optCom2 As System.Windows.Forms.RadioButton
    Friend WithEvents optCom1 As System.Windows.Forms.RadioButton
    Friend WithEvents txtTimeout As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtBaudrate As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkAutorx As System.Windows.Forms.CheckBox
    Friend WithEvents chkRTS As System.Windows.Forms.CheckBox
    Friend WithEvents chkDTR As System.Windows.Forms.CheckBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Private WithEvents moRS232 As rs232
    Private mlTicks As Long
    Private Delegate Sub CommEventUpdate(ByVal source As rs232, ByVal mask As rs232.EventMasks)
    Public Errore_Foto As String
    Public Errore_Encoder As String
    Public Errore_UV As String
    Public Errore_Cis As String
    Public Errore_Tape_Mag As String
    Public Errore_Foto_Trasparenza As String
    Public Errore_SPI As String
    Public Errore_Sd_Card As String
    Public Open_Com As Integer
    Public Test As Boolean
    Public Baudrate As Integer
    Private Const PURGE_RXCLEAR As Integer = &H8


    Private Sub Principale_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub Principale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not moRS232 Is Nothing Then
            '// Disables Events if active
            moRS232.DisableEvents()
            If moRS232.IsOpen Then moRS232.Close()
        End If
        Tasto = 13

        For Each p As Process In Process.GetProcesses()
            If p.Id = Process.GetCurrentProcess().Id Then
                'MessageBox.Show("Nome Processo: " & Process.GetCurrentProcess().ProcessName)
                p.CloseMainWindow()

                p.Close()

            End If
        Next

        Application.ExitThread()
        Application.Exit()

        End

    End Sub

    Private Sub Principale_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Tasto = (e.KeyCode)
    End Sub

    Private Sub Principale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '4.2 Verifica delle connessioni (RS232/RS485/USB 2.0)	10
        '4.3 Download di un CDF di test	11
        '4.4 Test micro-SD card interface	11
        '4.5 Test del generatore di corrente per il fotosensore di ingresso	11
        '4.6 Test dell’ encoder	12
        '4.7 Test dell’elettronica dei sistemi ottici	12
        '4.8 Test del sensore UVF + UVR	14
        '4.9 Verifica comunicazione con moduli periferici di TAPE e MAG	14
        '4.10 Verifica delle interfacce SPI	15
        '4.11 Verifica interfaccia SERDES	15
        '4.12 Test del sensore di temperatura	15
        '4.13 Verifica generatore di corrente ausiliario (trasparenza)	15
        '4.14 Scrittura del BoardID code (matricola della piastra) nella SPT	16
        '4.1 Trasferimento dei FW sulla piastra	10
        Try


            Label1.Top = Screen.PrimaryScreen.Bounds.Height / 15
            Label1.Width = Screen.PrimaryScreen.Bounds.Width - (Label1.Top * 2)
            Label1.Left = (Screen.PrimaryScreen.Bounds.Width - Label1.Width) / 2
            Label1.Height = Screen.PrimaryScreen.Bounds.Height / 7

            Button1.Width = Screen.PrimaryScreen.Bounds.Width / 6
            Button1.Height = Screen.PrimaryScreen.Bounds.Height / 15
            Button1.Left = Screen.PrimaryScreen.Bounds.Width / 2 - Button1.Width / 2
            Button1.Top = Screen.PrimaryScreen.Bounds.Height / 2

            Label2.Left = Screen.PrimaryScreen.Bounds.Width / 2 - Label2.Width / 2
            Label2.Top = Screen.PrimaryScreen.Bounds.Height / 2

            PictureBox1.Width = Screen.PrimaryScreen.Bounds.Width / 6
            PictureBox1.Left = Screen.PrimaryScreen.Bounds.Width / 2 - PictureBox1.Width / 2
            PictureBox1.Top = Label1.Top + Label1.Height + PictureBox1.Height



            ProgressBar1.Width = Label1.Width
            ProgressBar1.Top = Button1.Top
            ProgressBar1.Left = Label1.Left



            WorkDir = Application.StartupPath & "\"
            Dim com_port As String = Read_Set("Comunicazione", "Com")
            k_timeout = Read_Set("Comunicazione", "K_timeout")
            Cod_piastra = Read_Set("Configurazione", "Cod_Piastra")
            Me.Text = "Collaudo Piastra " & Cod_piastra
            Matr_attrezzatura = Read_Set("Configurazione", "Matr_Attrezzatura")

            Ripeti_trasp = False
            Attesa_Tasto = False
            'SerialPort1.PortName = "COM" & com_port
            Open_Com = (com_port)
            Debug_test = Read_Set("Debug", "Attendi_tasto")
            Attesa_Tasto = Read_Set("Debug", "Attendi_tasto")
            Debug_pausa = Read_Set("Debug", "Pausa")
            Salta_DWL = Read_Set("Debug", "Salta_DWL")
            Salta_trasf_fw = Read_Set("Debug", "Salta_trasf_fw ")
            Ripeti_trasp = Read_Set("Debug", "Ripeti_Trasparenza")
            UVSoloCanaleSx = Read_Set("Configurazione", "UVSoloCanaleSx")

            Text1.Visible = False
        Catch ex As IO.FileLoadException
            MessageBox.Show(ex.Message, "Errore File INI")
            Application.Exit()
            End
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Errore")
            Application.Exit()
            End
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim aa
            Label2.Text = "FAIL"
            Label2.ForeColor = Color.Red

            Test = False
            Button1.Visible = False
            Button1.Enabled = False
            Dim msg As String
            Stringa_Salvataggio = ""
            Stringa_Salvataggio_Pattern_Preset = ""


inserisci_operatore:
            Text1.Text = ""

            If Piastra.Nome_operatore = "" Then
                Text1.Visible = True
                Text1.Focus()
                Scrivi("Inserire nome operatore e premere Invio", Color.Blue)
                Attendi_Invio()
                If Text1.Text.Length < 2 Then
                    msg = "Il nome operatore deve essere di almeno 2 caratteri, introdurlo nuovamente"  ' Define message.
                    MsgBox(msg, MsgBoxStyle.Critical)
                    GoTo Inserisci_operatore
                End If
                Piastra.Nome_operatore = Text1.Text

            End If


Inserisci_matricola:
            Text1.Visible = True
            Text1.Focus()
            Text1.Text = ""
            Text1.Focus()

            Scrivi("Inserire matricola piastra e premere Invio", Color.Blue)
            Attendi_Invio()

            Text1.Text = Text1.Text.ToUpper

            'davide SN PIASTRA 
            If Len(Text1.Text) <> 16 Then
                msg = "La matricola lettore deve obbligatoriamente essere composta di 16 caratteri, introdurla nuovamente"  ' Define message.
                MsgBox(msg, MsgBoxStyle.Critical)
                GoTo Inserisci_matricola
            End If

            If Asc(Mid$(Text1.Text, 1, 1)) < 65 Or Asc(Mid$(Text1.Text, 1, 1)) > 90 Then

                msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                MsgBox(msg, MsgBoxStyle.Critical)
                GoTo Inserisci_matricola

            End If
            Dim kkk As Integer
            For a = 2 To 11
                kkk = Asc(Mid$(Text1.Text, a, 1))
                If kkk < 48 Or kkk > 57 Then

                    msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                    MsgBox(msg, MsgBoxStyle.Critical)
                    GoTo Inserisci_matricola
                End If
            Next

            aa = Val(Mid$(Text1.Text, 9, 2))

            If Val(Mid$(Text1.Text, 9, 2)) < 1 Or Val(Mid$(Text1.Text, 9, 2)) > 53 Then 'validità dalla settimana 1 alla settimana 53

                msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                MsgBox(msg, MsgBoxStyle.Critical)
                GoTo Inserisci_matricola
            End If

            For a = 12 To 16
                kkk = Asc(Mid$(Text1.Text, a, 1))
                If kkk < 48 Or kkk > 57 And kkk < 65 Or kkk > 70 Then

                    msg = "La matricola presenta caratteri non validi, introdurla nuovamente"  ' Define message.
                    MsgBox(msg, MsgBoxStyle.Critical)
                    GoTo Inserisci_matricola
                End If
            Next

            Piastra.Matricola_Piastra = Text1.Text
            Baudrate = 19200

            'close_port()
            open_port()
            moRS232.Dtr = 1
            Text1.Visible = False
            Text1.Text = ""


            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Collegamento Piastra"
            'Attendi_Invio()

            Scrivi("A piastra spenta, effettuare tutti i collegamenti fra la piastra in prova ed il sistema di test ed inserire RS20-JTAG in J4 " & Chr(13) & "Alimentare la piastra (Pulsante verde), attendere 2 sec e premere un tasto", Color.Blue)
            Attendi_Tasto()

            Scrivi("Connessione Automatica in corso....", Color.Black)

            If moRS232.IsOpen = False Then moRS232.Open()
connessione:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Connessione"
            Scrivi("Connessione in corso", Color.Black)
            No_Timeout = True
            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Inserimento dati"
            If Connessione_Automatica() <> 16 Then
                Scrivi(Stringa_di_connessione, Color.Red)
                Attendi_Tasto()
                If Tasto <> 27 Then GoTo connessione
                Stringa_Salvataggio = "Fail | "
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                No_Timeout = False
                GoTo fine
            End If
            No_Timeout = False
            Stringa_Salvataggio = " Pass | "
            'If Salta_trasf_fw = True Then Stringa_Salvataggio = Stringa_Salvataggio & "NA | NA |" : GoTo Test_com



Test_SD:
            If Read_Set("Configurazione", "TestMicroSD") = "1" Then
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test SD Card"

                If Test_Sd_Card() = False Then
                    Scrivi(Stringa_Errore, Color.Red)
                    Attendi_Tasto()
                    If Tasto <> 27 Then GoTo Test_SD
                    Stringa_Salvataggio = Stringa_Salvataggio & "Fail_ " & Errore_Sd_Card
                    Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                    GoTo fine
                End If
                Stringa_Salvataggio = Stringa_Salvataggio & "Pass | "
                Debug_test_control()
            End If
trasf_fw:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Trasferimento FW"

            moRS232.Dtr = 1
            If Trasferimento_dei_FW_sulla_piastra() = False Then
                Scrivi("Trasferimento dei FW sulla piastra Fallito! " & Stringa_Errore, Color.Red)
                Attendi_Tasto()
                If Tasto <> 27 Then GoTo trasf_fw
                Stringa_Salvataggio = Stringa_Salvataggio & "Fail | "
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass | "

            Debug_test_control()
Test_com:

            moRS232.Dtr = 0

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test Seriale"

            If Test_comunicazione() = False Then
                Test_comunicazione()
                Scrivi("Test comunicazione Fallito! " & Stringa_Errore, Color.Red)
                Attendi_Tasto()
                If Tasto <> 27 Then GoTo Test_com
                Stringa_Salvataggio = Stringa_Salvataggio & "Fail | "
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

            Stringa_Salvataggio = Stringa_Salvataggio & "Pass | "
            Debug_test_control()

            If Salta_DWL = True Then Stringa_Salvataggio = Stringa_Salvataggio & "NA | " : GoTo foto_in

dwl_rif:
            Dim Nome_riferimento As String = Read_ParPiastra("Riferimenti attrezzo di collaudo", "TEST_CDF_FILENAME")

            If DownLoad(WorkDir & "Riferimenti\" & Nome_riferimento, Label1, ProgressBar1) = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                ProgressBar1.Visible = False
                Call MySleep(1000)
                Call Cambio_Velocita_Seriale(1, True)
                Call MySleep(500)
                attendi_per_t("41", 1000, 10000)
                If Tasto <> 27 Then GoTo dwl_rif
                Stringa_Salvataggio = Stringa_Salvataggio & "Fail | "
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass | "
            Debug_test_control()



foto_in:
            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test Foto Ingresso"

            'If Test_Cis() = False Then
            If Test_Foto_Ingresso() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                If Tasto <> 27 Then GoTo foto_in
                Stringa_Salvataggio = Stringa_Salvataggio & "Fail_ " & Errore_Foto
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass | "
            Debug_test_control()
            Grid1.Rows.Clear()
            Grid1.Visible = False

test_foto_trasp:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test Foto Trasparenza"

            If Test_Foto_Trasparenza() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo test_foto_trasp
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_Foto_Trasparenza
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

            Debug_test_control()
            Grid1.Rows.Clear()
            Grid1.Visible = False

test_enc:
            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test Encoder"

            If Test_Encoder() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo Test_enc
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_Encoder
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

            Debug_test_control()
test_uv:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test UV"

            If Test_UV() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo test_uv
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_UV
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

test_cis:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test CIS"

            If Test_Cis() = False Then

                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo test_cis
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_Cis
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If
            Debug_test_control()

test_tape_mag:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test Tape e Mag"

            If Test_Tape_Mag() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo test_tape_mag
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_Tape_Mag
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

            Debug_test_control()

test_interfacce_SPI:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test SPI"

            If Test_SPI() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo test_interfacce_SPI
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_SPI
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

            Debug_test_control()

            'test_foto_trasp_1:

            '            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Test Foto Trasparenza"

            '            If Test_Foto_Trasparenza() = False Then
            '                Scrivi(Stringa_Errore, Color.Red)
            '                Attendi_Tasto()
            '                Grid1.Visible = False
            '                If Tasto <> 27 Then GoTo test_foto_trasp
            '                Stringa_Salvataggio = Stringa_Salvataggio & Errore_Foto_Trasparenza
            '                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
            '                GoTo fine
            '            End If

            '            Debug_test_control()
Scrivi_matr:

            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Scrittura e verifica matricola"

            Dim matr_piastra = stringa_ricezione(UCase(Piastra.Matricola_Piastra & "0000000000000000"))

            Traspsend("D36E" + matr_piastra, 1, 3000)
            Traspsend("D36F", 32, 3000)
            aa = Mid(Traspric, 1, 16)
            If Mid(Traspric, 1, 16) <> Piastra.Matricola_Piastra Then
                Scrivi("Scrittura matricola fallita, premere un tasto per ripetere la prova, ESC per uscire", Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                If Tasto <> 27 Then GoTo scrivi_matr
                Stringa_Salvataggio = Stringa_Salvataggio & "Fail | "
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass | "

Ver_fw:

            If Verifica_FW() = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Grid1.Visible = False
                Stringa_Salvataggio = Stringa_Salvataggio & Errore_FW
                Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
                GoTo fine
            End If

            Debug_test_control()
            Test = True
            Label2.Text = "PASS"
            Label2.ForeColor = Color.Green
            Label2.Visible = True
            Me.Text = "Collaudo Piastra " & Cod_piastra & " - Fine Test"
            Scrivi("Test piastra superato, spegnere l'apparecchiatura di test, scollegare la piastra e premere un tasto.", Color.Green)
            salva_dati(Test)
            Attendi_Tasto()

            If (Ripeti_trasp = True) Then

                Dim result As Integer = MessageBox.Show("Vuoi ripetere il test trasparenza ? ", "", MessageBoxButtons.YesNo)
                If result = DialogResult.Cancel Then

                ElseIf result = DialogResult.No Then

                ElseIf result = DialogResult.Yes Then
                    Piastra.Matricola_Piastra = Text1.Text
                    Baudrate = 19200

                    close_port()
                    open_port()
                    moRS232.Dtr = 1
                    Text1.Visible = False
                    Text1.Text = ""

                    'Me.Text = "Collaudo Piastra " & Cod_piastra & " - Collegamento Piastra"

                    'Scrivi("A piastra spenta, effettuare tutti i collegamenti fra la piastra in prova ed il sistema di test ed inserire RS20-JTAG in J4 " & Chr(13) & "Alimentare la piastra (Pulsante verde), attendere 2 sec e premere un tasto", Color.Blue)
                    'Attendi_Tasto()

                    Scrivi("Connessione Automatica in corso....", Color.Black)

                    If moRS232.IsOpen = False Then moRS232.Open()


                    Me.Text = "Collaudo Piastra " & Cod_piastra & " - Connessione"
                    Scrivi("Connessione in corso", Color.Black)
                    No_Timeout = True
                    No_Timeout = False
                    Stringa_Salvataggio = " Pass | "
                    'If Salta_trasf_fw = True Then Stringa_Salvataggio = Stringa_Salvataggio & "NA | NA |" : GoTo test_com
                    Label2.Visible = False
                    Test_Foto_Trasparenza()

                End If
            End If
fine:
        Catch ex As IO.IOException
            MessageBox.Show("Eccezione:" & ex.Message, "Problemi di comunicazione")
            Stringa_Salvataggio = Stringa_Salvataggio & ex.Message & " | "
        Catch ex As Exception
            MessageBox.Show("Eccezione:" & ex.Message, "Errore")
            Stringa_Salvataggio = Stringa_Salvataggio & ex.Message & " | "
        Finally

            If Test = False Then
                salva_dati(Test)
                Scrivi("Test piastra fallito, premere un tasto per continuare", Color.Red)
                Label2.Visible = True
                Attendi_Tasto()
            End If

            Scrivi("", Color.Black)

            'If (Ripeti_trasp = False) Then
            Label2.Visible = False
            Button1.Visible = True
            Button1.Enabled = True
            Button1.Focus()
            Text1.Visible = False
            Grid1.Visible = False

            'End If
            moRS232.Close()

        End Try
    End Sub

    Function Trasferimento_dei_FW_sulla_piastra() As Boolean
        Dim aa
        '        Per fare questo sono necessari i seguenti passi:
        '1.A piastra spenta, effettuare tutti i collegamenti fra la piastra in prova ed il sistema di test 
        '  ed inserire RS20-JTAG in J4 (il piastrino deve avere il jumper inserito nell’apposito connettore
        '  affinché il boot avvenga dallo stesso);
        '2.Alimentare la piastra con una tensione continua da 18 a 32 V;
        '3.Attendere 2 sec ed eseguire il polling con la richiesta di stato Status (F3) per verificare il 
        '  completamento della fase di inizializzazione;
        '4.Verifica con la richiesta GetHWConfig (D3 24) che si stia facendo il boot da RS20-JTAG 
        '  (se così non fosse, eseguendo l’operazione successiva ci sarebbe il rischio di invalidare 
        '  il contenuto della flash memory sull’ adattatore);
        '5.eseguire il trasferimento del FW di test sulla piastra con il cmd FwRecovery (D3 31) ed 
        '  effettuare il polling con il cmd Status per verificarne la corretta esecuzione;
        '6.eseguire il trasferimento della SPT di test sulla piastra con il cmd SptCopy (D3 3F 02) ed
        '  effettuare il polling con il cmd Status per verificarne la corretta esecuzione;
        '7.Spegnere e commutare la RS20-JTAG. Verificare con il cmd GetHWConfig che il codice in 
        '  esecuzione sia quello sulla MB e non quello su JTAG. Il modulo ripartirà con la porta seriale 
        '  impostata nel seguente modo: 19200 bps - N - 8 – 2, risposta automatica attivata ed uso del 
        '  clock esterno (encoder);

        Dim RS2x_FW_NAME_HOST_BOOT As String = Read_ParPiastra("Firmware attrezzo di collaudo", "RS2x_FW_NAME_HOST_BOOT")
        Dim RS2x_FW_NAME_DSP_BOOT As String = Read_ParPiastra("Firmware attrezzo di collaudo", "RS2x_FW_NAME_DSP_BOOT")
        Dim RS2x_FW_NAME_FPGA_BOOT As String = Read_ParPiastra("Firmware attrezzo di collaudo", "RS2x_FW_NAME_FPGA_BOOT")
        If Salta_trasf_fw = True Then Stringa_Salvataggio = Stringa_Salvataggio & "NA | NA |" : GoTo trasferimentoSPT

        Scrivi("Trasferimento FW in corso, attendere...", Color.Black)
        Trasferimento_dei_FW_sulla_piastra = False
        Reboot()
        traspsend("D324", 8, 4000)
        aa = stringa_ricezione(Traspric)

        RS2x_FW_NAME_HOST_BOOT = Read_ParPiastra("Firmware attrezzo di collaudo", "RS2x_FW_NAME_HOST_BOOT")

        If Mid(aa, 14, 1) = 0 Then
            Stringa_Errore = "Piastrino RS20-JTAG non rilevato, premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

        No_Timeout = True
        Call Traspsend("E32B", 13, 1000)
        No_Timeout = False

        If Traspric.Length <> 13 Then
            Traspsend("E320", 13, 2000)
        End If

        If RS2x_FW_NAME_HOST_BOOT <> Mid(Traspric, 1, 8) Then
            Stringa_Errore = "Il Firmware presente sul Piastrino RS20-JTAG (" & Traspric & ") non corrisponde a quello letto nel file INI (" & RS2x_FW_NAME_HOST_BOOT & "), premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

        Traspsend("D331", 0, 0)

        Dim sta = Now

        MySleep(2000)

        If attendi_per_t("41", 500, 300000) = False Then
            Stringa_Errore = "Timeout di scrittura FW superato, procedura fallita, premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If
        Dim sto = Now
        Dim timeout = sto - sta

        Traspsend("F3", 1, 1000)

        If stringa_ricezione(Traspric) = "12" Then
            Stringa_Errore = "Stato = '" & stringa_ricezione(Traspric) & "' (flash write error) dopo Trasferimento dei FW sulla piastra (D331). Tempo impiegato = " & timeout.TotalSeconds() & " secondi, premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

trasferimentoSPT:
        Scrivi("Trasferimento SPT in corso, attendere...", Color.Black)
        Traspsend("D33F02", 0, 0)
        MySleep(2000)

        attendi_per_t("41", 350, 300000)

        moRS232.Dtr = 0

        Reboot(True, 1)

        MySleep(500)

        Auto_Answ_ON()

        No_Timeout = True
        Call Traspsend("E32B", 13, 1000)
        No_Timeout = False

        If Traspric.Length <> 13 Then
            Traspsend("E320", 13, 2000)
        End If

        If Mid(Traspric, 1, 8) <> RS2x_FW_NAME_HOST_BOOT Then
            Stringa_Errore = "Il Firmware Host caricato sulla piastra (" & Traspric & ") non corrisponde a quello letto nel file INI (" & RS2x_FW_NAME_HOST_BOOT & "), premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

        Traspsend("E322", 13, 2000)

        If Mid(Traspric, 1, 8) <> RS2x_FW_NAME_FPGA_BOOT Then
            Stringa_Errore = "Il Firmware caricato sulla piastra (" & Traspric & ") non corrisponde a quello letto nel file INI (" & RS2x_FW_NAME_FPGA_BOOT & "), premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

        Traspsend("E321", 13, 2000)

        If Mid(Traspric, 1, 8) <> RS2x_FW_NAME_DSP_BOOT Then
            Stringa_Errore = "Il Firmware caricato sulla piastra (" & Traspric & ") non corrisponde a quello letto nel file INI (" & RS2x_FW_NAME_DSP_BOOT & "), premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

        Traspsend("D324", 8, 4000)
        aa = stringa_ricezione(Traspric)

        If attendi_per_t("41", 500, 5000) = False Then
            Stringa_Errore = "Timeout di scrittura SPT scaduto, procedura fallita, premere un tasto per ripetere, 'Esc' per uscire"""
            Exit Function
        End If

        If Mid(aa, 14, 1) <> 0 Then
            Stringa_Errore = "Il piastrino RS20-JTAG risulta ancora connesso, procedura fallita, premere un tasto per ripetere, 'Esc' per uscire"""
            Exit Function
        End If

        '1.Eseguire il backup della SPT dalla E2prom alla flash con il cmd SptCopy (D3 3F 01) ed 
        'effettuare il polling con il cmd Status per verificarne la corretta esecuzione.

        Traspsend("D33F01", 0, 0)

        If attendi_per_t("41", 500, 5000) = False Then
            Stringa_Errore = "Timeout backup della SPT dalla E2prom alla flash scaduto, procedura fallita, premere un tasto per ripetere, 'Esc' per uscire"""
            Exit Function
        End If

        Stringa_Errore = ""
        Trasferimento_dei_FW_sulla_piastra = True


    End Function

    Function USB_Array_to_String(ByVal buf As Byte()) As String
        USB_Array_to_String = ""
        If buf.Length > 0 Then
            For a = 0 To buf.Length - 1
                USB_Array_to_String = USB_Array_to_String & Chr(buf(a))
            Next
        End If
    End Function

    Function Test_comunicazione() As Boolean
        'Il funzionamento della connessione RS232 si verifica immediatamente nel momento in cui si esegue il trasferimento dei FW dalla flash di boot. Un po’ più articolata è la verifica di funzionamento della RS485.

        'Leggere da file INI i seguenti parametri:
        'la stringa di controllo: RS485_CHECK_STRING (nel f.to HEX dd.dd.dd…).
        'La stringa di verifica:  RS485_VERIFY_STRING (nel f.to HEX dd.dd.dd…).
        'Il limite ad entrambe le stringhe è posto a 32 bytes

        'In seguito eseguire la sequenza riportata sotto:

        '1.Calcolo dei dati presenti nella stringa di controllo e trasformazione della stessa in codici esadecimali splitted (es: la stringa 41.45.5A viene trasformata in 04 01 04 05 05 0A). Chiaramente la lunghezza della stringa risultante è il doppio di quella originaria.
        '2.Invio della stringa di controllo con il comando TestRS485 (D3 27 01).
        '3.A questo punto attendere 3 secondi, svuotare il buffer di ricezione della seriale e verificare che lo stato del lettore sia diverso da BUSY (usare la richiesta di stato Status – F3);
        '4.Eseguire la richiesta GetRS485Str (D3 27 02) e recuperare la stringa di verifica sempre nel formato byte splitted. 
        '5.Ricomporre la stringa in formato HEX dd.dd.dd…e verificare che corrisponda a quella di verifica RS485_VERIFY_STRING.

        'Per quanto riguarda l’interfaccia USB, e necessario usare il cmd 0x20 (vedi Allegato B).
        If Read_Set("Configurazione", "VerificaRS485") = "1" Then
#Region "PMA-E-002"
            Dim RS485_CHECK_STRING As String
            Dim RS485_VERIFY_STRING As String
            Dim aa As String
            Dim NByte As String

            Scrivi("Verifica 485 in corso....", Color.Black)

            Test_comunicazione = False

            RS485_CHECK_STRING = Read_ParPiastra("RS485 attrezzo di collaudo", "RS485_CHECK_STRING")
            RS485_VERIFY_STRING = Read_ParPiastra("RS485 attrezzo di collaudo", "RS485_VERIFY_STRING")

            aa = RS485_CHECK_STRING.Length
            NByte = dec_hex(RS485_CHECK_STRING.Length / 2)

            RS485_CHECK_STRING = METTE_0(Trim(RS485_CHECK_STRING))
            aa = "D32701" & NByte & RS485_CHECK_STRING
            Traspsend("D32701" & NByte & RS485_CHECK_STRING, 0, 3000)

            Call Traspsend("F3", 1, 1000)

            If stringa_ricezione(Traspric) = "41" Then
                Stringa_Errore = "Verifica RS485 Fallita, Stato 41"
                Exit Function
            End If

            Traspsend("D32702" & NByte, RS485_VERIFY_STRING.Length, 5000)
            aa = stringa_ricezione(Traspric)
            If TOGLIE_0(stringa_ricezione(Traspric)) <> RS485_VERIFY_STRING Then
                Stringa_Errore = "Verifica RS485 Fallita, la stringa di verifica non corrisponde alla risposta letta, premere un tasto per ripetere, 'Esc' per uscire"
                Exit Function
            End If
        End If
#End Region

        'Dim ama As String
        'ama = Encoding.ASCII.GetString(USBB("20", 2, 12, False), 0, 12)
        'Dim ama As Byte() = USBB("20", 2, 12, False)
        Dim ama As Byte() = USBB("20", 12, 12, False)

        'If ama <> "123456 prova" Then
        If USB_Array_to_String(ama) <> "123456 prova" Then
            Stringa_Errore = "Verifica USB Fallita, la stringa di verifica non corrisponde alla risposta letta, premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If

        Test_comunicazione = True


    End Function

    Private Sub Button1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Button1.KeyPress
        Tasto = Asc(e.KeyChar)
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub



    Public Function DownLoad(ByVal Nome_File As String, ByVal Etichetta As Object, ByVal Progress As Object) As Boolean

        Dim Tipo As String
        DownLoad = False
        Progress.Value = 0

        Dim nome_file_dwl As String = Nome_File.Substring(Nome_File.LastIndexOf("\") + 1) & ".hex"
        Me.Text = "Collaudo Piastra " & Cod_piastra & " - Download " & nome_file_dwl
        Etichetta.Text = "Analisi del file in corso... "
        Auto_Answ_ON()

        'Pass the file path and the file name to the StreamReader constructor.

        Dim readText() As String = File.ReadAllLines(Nome_File & ".Hex")

        'codice di sblocco per rs2x
        traspsend("D37B040605070505040E040C040F0403040B", 0, 0)
        If attendi_per_t("41", 500, 5000) = False Then
            Stringa_Errore = "Fallito invio stringa di sblocco, premere un tasto per ripetere, 'Esc' per uscire"
        End If

        Tipo = Mid(readText(0).ToString, 10, 2)

        If Tipo = "52" Then
            'Call traspsen("D3500" + Trim(Str(Banco)), 1, 3)
        End If

        If Tipo = "58" Then
            'D3 7B 07 08 06 09 07 05 06 0E 06 0C 06 0F 06 03 06 0B (XIL_UNLOCK)
            Call traspsend("D37B070806090705060E060C060F0603060B", 0, 0)
            If attendi_per_t("41", 500, 5000) = False Then
                Stringa_Errore = "Fallito invio stringa di sblocco XIL_UNLOCK" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"""
                Exit Function
            End If
        End If

        If Tipo = "42" Then
            Call traspsend("D3790602060F0705060E060C060F0603060B", 0, 0)
            If attendi_per_t("41", 500, 5000) = False Then
                Stringa_Errore = "Fallito invio stringa di sblocco BOOT_UNLOCK" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"""
                Exit Function
            End If
        End If

        Progress.Maximum = readText.Length - 1

        Etichetta.Text = "Cambio velocità seriale in corso"
        If Cambio_Velocita_Seriale(4, True) = False Then
            Stringa_Errore = "Fallito cambio di velocità" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"""
            Exit Function
        End If

        Etichetta.Text = "Download in corso.... "
        Progress.visible = True
        For a = 0 To readText.Length - 1
            Application.DoEvents()
            Progress.value = a
            Call traspsend("93190000000000000000" & stringa_ricezione(readText(a)), 1, 18000)
            'Call traspsend(stringa_ricezione(readText(a)), 1, 18000)

            If stringa_ricezione(Traspric) = "04" Or stringa_ricezione(Traspric) = "" Then
                Stringa_Errore = "Download fallito stato = " & stringa_ricezione(Traspric) & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"""

                Exit Function

            End If
            If readText(a) = ":00000001FF" Then Exit For
        Next

        Scrivi("Download terminato attendere ripristino Impostazioni", Color.Black)
        Progress.visible = False
        If Tipo <> "52" Then
            Call MySleep(3000)
        Else
            Call MySleep(1000)
        End If

        If (stringa_ricezione(Traspric)) = "0F" Then

            Stringa_Errore = "Download fallito, stato = '0F'" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function

        End If

        If (stringa_ricezione(Traspric)) = "2A" Then

            Stringa_Errore = "Download fallito, stato = '2A'" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function

        End If

        If (stringa_ricezione(Traspric)) <> "04" Then
            If Tipo <> "52" Then
                Cambio_Velocita_Seriale(1, False)

            Else
                If Cambio_Velocita_Seriale(1, True) = False Then
                    Stringa_Errore = "Fallito cambio di velocità" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"
                    Exit Function
                End If
            End If

            If attendi_per_t("41", 1000, 10000) = False Then
                Stringa_Errore = "Fallita riconnessione dopo il download" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"""
                Exit Function
            End If


            Call traspsend("D31300", 0, 0)
            DownLoad = True
            Exit Function
        Else
            Stringa_Errore = "Download fallito, stato = '04'" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"
            Exit Function
        End If


    End Function

    Public Function Test_Foto_Ingresso() As Boolean
        '        Leggere da file INI i seguenti parametri:
        'i riferimenti FOTIN_REF_DARK0, FOTIN_REF_LIGHT1, FOTIN_REF_DARK1, FOTIN_REF_LIGHT2 e FOTIN_REF_DARK2;
        'i valori di corrente FOTIN_CURRENT1, FOTIN_CURRENT2.

        'Le operazioni da eseguire sono le seguenti:	
        '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 
        '2.Impostare con il comando MemWrite la corrente del foto emettitore a 0 (zero) ed attendere un minimo di 5 msec;
        '3.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore FOTO_REF_DARK0 ed attendere un minimo di 5 msec;
        '4.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1 (NOTE_IN);

        Dim aa As String
        '[Fotosensore Ingresso attrezzo collaudo]
        Dim FOTIN_REF_DARK0 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_REF_DARK0")

        Dim FOTIN_CURRENT1 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_CURRENT1")

        Dim FOTIN_REF_DARK1 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_REF_DARK1")
        Dim FOTIN_REF_DARK1_NEW As Integer = 250

        Dim FOTIN_REF_LIGHT1 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_REF_LIGHT1")

        Dim FOTIN_CURRENT2 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_CURRENT2")
        Dim FOTIN_REF_DARK2 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_REF_DARK2")
        Dim FOTIN_REF_LIGHT2 As Integer = Read_ParPiastra("Fotosensore Ingresso attrezzo collaudo", "FOTIN_REF_LIGHT2")
        Grid1.ColumnCount = 4
        Grid1.Rows.Clear()
        Grid1.Rows.Add("#Verifica", "REF", "CURRENT", "FOTIN")
        Grid1.Visible = True
        Scrivi("Reboot in corso....", Color.Black)
        Reboot()
        Test_Foto_Ingresso = False
        Scrivi("Test Fotosensore di ingresso in corso....", Color.Black)
        Call traspsend("D3070400", 1, 200)

        Mem_Write(1, "84000000", "00000004", "00010000")
        'MySleep(5)
        MySleep(150)
        Mem_Write(1, "84000000", "00000004", "00020000")
        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 6) & "00"

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_DARK0) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")
        Grid1.Rows.Add("1", "DARK0=" & FOTIN_REF_DARK0, "CUR1=" & FOTIN_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> 1 Then
            Stringa_Errore = "Test Fotosensore Ingresso fallito, fallita verifica 1, premere un tasto per ripetere, Esc per uscire"
            Call traspsend("D3070401", 1, 200)
            Call traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto = "Step1 | "
            Exit Function

        End If



        '5.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore FOTO_REF_DARK1 ed attendere un minimo di 5 msec;
        '6.Impostare con il comando MemWrite la corrente del foto emettitore a FOTIN_CURRENT1 ed attendere un minimo di 5 msec;
        '7.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_DARK1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        ' MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 6) & dec_hex(FOTIN_CURRENT1)

        Mem_Write(1, "84000000", "00000004", aa)

        ' MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        Grid1.Rows.Add("2", "DARK1=" & FOTIN_REF_DARK1, "CUR1=" & FOTIN_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> 1 Then
            Stringa_Errore = "Test Fotosensore Ingresso fallito, fallita verifica 2, premere un tasto per ripetere, Esc per uscire"

            Call traspsend("D3070401", 1, 200)
            Call traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto = "Step2 | "
            Exit Function

        End If

        '5.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore FOTO_REF_DARK1 ed attendere un minimo di 5 msec;
        '6.Impostare con il comando MemWrite la corrente del foto emettitore a FOTIN_CURRENT1 ed attendere un minimo di 5 msec;
        '7.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;

        'aggiungo un ulteriore ref a 250 per problematiche..ma non guardo il valore di uscita
        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_DARK1_NEW) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 6) & dec_hex(FOTIN_CURRENT1)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        'If Mid(stringa_ricezione(Traspric), 8, 1) <> 1 Then
        '    Stringa_Errore = "Test Fotosensore Ingresso fallito, fallita verifica 2, premere un tasto per ripetere, Esc per uscire"

        '    Call Traspsend("D3070401", 1, 200)
        '    Call Traspsend("D30703", 0, 0)
        '    attendi_per("41")
        '    Errore_Foto = "Step2 | "
        '    Exit Function

        'End If


        '8.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore FOTO_REF_LIGHT1 ed attendere un minimo di 5 msec;
        '9.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 0;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_LIGHT1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        Grid1.Rows.Add("3", "LIGHT1=" & FOTIN_REF_LIGHT1, "CUR1=" & FOTIN_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> 0 Then
            Stringa_Errore = "Test Fotosensore Ingresso fallito, fallita verifica 3, premere un tasto per ripetere, Esc per uscire"
            Call traspsend("D3070401", 1, 200)
            Call traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto = "Step3 | "
            Exit Function

        End If

        '10.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore FOTO_REF_DARK2 ed attendere un minimo di 5 msec;
        '11.Impostare con il comando MemWrite la corrente del foto emettitore a FOTIN_CURRENT2 ed attendere un minimo di 5 msec;
        '12.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;
        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 6) & dec_hex(FOTIN_CURRENT2)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_DARK2) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        Grid1.Rows.Add("4", "DARK2=" & FOTIN_REF_DARK2, "CUR2=" & FOTIN_CURRENT2, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> 1 Then
            Stringa_Errore = "Test Fotosensore Ingresso fallito, fallita verifica 4, premere un tasto per ripetere, Esc per uscire"
            Call traspsend("D3070401", 1, 200)
            Call traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto = "Step4 | "
            Exit Function

        End If
        '13.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore FOTO_REF_LIGHT2 ed attendere un minimo di 5 msec;
        '14.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 0;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_LIGHT2) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        'MySleep(5)
        MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        Grid1.Rows.Add("5", "LIGHT2=" & FOTIN_REF_LIGHT2, "CUR2=" & FOTIN_CURRENT2, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> 0 Then
            Stringa_Errore = "Test Fotosensore Ingresso fallito, fallita verifica 5, premere un tasto per ripetere, Esc per uscire"
            Call traspsend("D3070401", 1, 200)
            Call traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto = "Step5 | "
            Exit Function

        End If
        Call traspsend("D3070401", 1, 200)
        Call traspsend("D30703", 0, 0)
        attendi_per("41")
        Scrivi("Test Fotosensore di ingresso Superato", Color.Green)
        Test_Foto_Ingresso = True
        '15.Riabilitazione del sincronismo di acquisizione EnableNoteIn(ON).

        'spostati dopo il debug_test_control per vedere i dati in fase di debug
        'Grid1.Rows.Clear()
        'Grid1.Visible = False 
    End Function

    Public Function Test_Encoder() As Boolean
        'L'obiettivo del test è quello di verificare il funzionamento del sistema di acquisizione del sincronismo 
        'relativo al trasporto.

        'Leggere da file INI i seguenti parametri:
        'La frequenza di riferimento ENC_FREQ_NOM e 
        'La sua tolleranza ENC_FREQ_TOLERANCE.

        'A questo punto è necessario attivare l’impulso di sincronismo in ingresso alla MB ed eseguire la seguente 
        'procedura:
        '1.Esecuzione della richiesta GetTransportCheck (D3 1A 05) ed attesa della risposta.
        '2.La risposta alla richiesta precedente consiste nei seguenti tre dati: velocità minima, velocità media e 
        'velocità massima riscontrate durante il periodo di osservazione. 
        'Questi dati devono obbligatoriamente essere inclusi nel range ENC_FREQ_NOM ± ENC_FREQ_TOLERANCE.

        '(Valutare se è il caso di ricorrere ad una procedura più corta)

        'Da notare che l’impulso di sincronismo deve rimanere attivato per tutta la durata del collaudo funzionale 
        'della piastra.
        Dim ENC_FREQ_NOM As Integer
        Dim ENC_FREQ_TOLERANCE As Integer
        Dim aa As String
        Dim Test As Boolean = True
        Scrivi("Test Encoder....", Color.Black)

        Test_Encoder = True
        cancella_griglia(Grid1)

        ENC_FREQ_NOM = Read_ParPiastra("Encoder attrezzo collaudo", "ENC_FREQ_NOM")
        ENC_FREQ_TOLERANCE = Read_ParPiastra("Encoder attrezzo collaudo", "ENC_FREQ_TOLERANCE")

        Call traspsend("D31A05", 12, 1000)

        aa = TOGLIE_0(stringa_ricezione(Traspric))

        Dim Min As Integer = "&h" & Mid$(aa, 1, 4)
        Dim Med As Integer = "&h" & Mid$(aa, 5, 4)
        Dim Max As Integer = "&h" & Mid$(aa, 9, 4)
        Grid1.ColumnCount = 3
        Grid1.RowCount = 3
        Grid1.Item(0, 0).Value = "Velocità Min"
        Grid1.Item(0, 1).Value = "Velocità Med"
        Grid1.Item(0, 2).Value = "Velocità Max"

        Grid1.Item(1, 0).Value = Min
        Grid1.Item(1, 1).Value = Med
        Grid1.Item(1, 2).Value = Max

        Grid1.Item(2, 0).Value = "[" & ENC_FREQ_NOM - ENC_FREQ_TOLERANCE & "-" & ENC_FREQ_NOM + ENC_FREQ_TOLERANCE & "]"
        Grid1.Item(2, 1).Value = "[" & ENC_FREQ_NOM - ENC_FREQ_TOLERANCE & "-" & ENC_FREQ_NOM + ENC_FREQ_TOLERANCE & "]"
        Grid1.Item(2, 2).Value = "[" & ENC_FREQ_NOM - ENC_FREQ_TOLERANCE & "-" & ENC_FREQ_NOM + ENC_FREQ_TOLERANCE & "]"
        Grid1.ClearSelection()

        Grid1.ForeColor = Color.Black

        Ridimensiona_Griglia(Grid1)

        If Min > ENC_FREQ_NOM + ENC_FREQ_TOLERANCE Or Min < ENC_FREQ_NOM - ENC_FREQ_TOLERANCE Then
            Grid1.Item(1, 0).Style.ForeColor = Color.Red
            Test = False
        Else
            Grid1.Item(1, 0).Style.ForeColor = Color.Green
        End If
        If Med > ENC_FREQ_NOM + ENC_FREQ_TOLERANCE Or Med < ENC_FREQ_NOM - ENC_FREQ_TOLERANCE Then
            Grid1.Item(1, 1).Style.ForeColor = Color.Red
            Test = False
        Else
            Grid1.Item(1, 1).Style.ForeColor = Color.Green
        End If

        If Max > ENC_FREQ_NOM + ENC_FREQ_TOLERANCE Or Max < ENC_FREQ_NOM - ENC_FREQ_TOLERANCE Then
            Grid1.Item(1, 2).Style.ForeColor = Color.Red
            Test = False
        Else
            Grid1.Item(1, 2).Style.ForeColor = Color.Green
        End If
        Grid1.Visible = True


        Application.DoEvents()

        Debug_test_control()

        If Test = True Then
            Scrivi("Test Encoder superato", Color.Green)
            Grid1.Visible = False
            Test_Encoder = True
            Application.DoEvents()
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass | " & Min & " | " & Med & " | " & Max & " | "
        Else
            Stringa_Errore = "Test Encoder Fallito, premere un tasto per ripetere, Esc per uscire"
            Test_Encoder = False
            Errore_Encoder = "Fail | " & Min & " | " & Med & " | " & Max & " | "
        End If


    End Function

    Public Function Test_UV() As Boolean
        Dim aa

        Dim UV_OFF_LOW As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_OFF_LOW")
        Dim UV_OFF_HIGH As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_OFF_HIGH")

        Dim UV_LEFT_IGEN_TOLERANCE As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_LEFT_IGEN_TOLERANCE")
        Dim UV_RIGHT_IGEN_TOLERANCE As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_RIGHT_IGEN_TOLERANCE")

        Dim UV_LEFT_RIFE_MIN As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_LEFT_RIFE_MIN")
        Dim UV_LEFT_RIFE_MED_LO As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_LEFT_RIFE_MED_LO")
        Dim UV_LEFT_RIFE_MED_HI As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_LEFT_RIFE_MED_HI")
        Dim UV_LEFT_RIFE_MAX As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_LEFT_RIFE_MAX")

        Dim UV_RIGHT_RIFE_MIN As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_RIGHT_RIFE_MIN")
        Dim UV_RIGHT_RIFE_MED_LO As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_RIGHT_RIFE_MED_LO")
        Dim UV_RIGHT_RIFE_MED_HI As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_RIGHT_RIFE_MED_HI")
        Dim UV_RIGHT_RIFE_MAX As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_RIGHT_RIFE_MAX")

        Dim Test As Boolean = True

        '1.Lettura dell’ offset elettrico: entrambi i dati relativi ai due canali UV devono essere compresi nel 
        'range [UV_OFF_LOW - UV_OFF_HIGH] (INI file). Per leggere l’offset usare il 
        'cmd GetUVOffset (D3 08 00 è 0x 0x 0y 0y), dove il valore xx è relativo al canale LEFT, 
        'mentre yy è relativo al canale RIGHT.

        Scrivi("Test lettura Offset Elettrico....", Color.Black)
        cancella_griglia(Grid1)

        Call traspsend("D30800", 4, 1000)
        aa = TOGLIE_0(stringa_ricezione(Traspric))

        Dim Uv_Off_left As Integer
        Dim Uv_Off_right As Integer

        Uv_Off_left = "&h" & Mid(aa, 1, 2)
        Uv_Off_right = "&h" & Mid(aa, 3, 2)
        Grid1.ColumnCount = 3
        Grid1.RowCount = 1
        Grid1.Item(0, 0).Value = "UV Offset Left"
        Grid1.Item(1, 0).Value = Uv_Off_left
        Grid1.Item(2, 0).Value = "[" & UV_OFF_LOW & "-" & UV_OFF_HIGH & "]"
        If Uv_Off_left > UV_OFF_HIGH Or Uv_Off_left < UV_OFF_LOW Then
            Test = False
            Grid1.Item(1, 0).Style.ForeColor = Color.Red
        Else
            Grid1.Item(1, 0).Style.ForeColor = Color.Green
        End If

        If UVSoloCanaleSx = False Then
            Grid1.RowCount = 2
            Grid1.Item(0, 1).Value = "UV Offset Right"
            Grid1.Item(1, 1).Value = Uv_Off_right
            Grid1.Item(2, 1).Value = "[" & UV_OFF_LOW & "-" & UV_OFF_HIGH & "]"
            If Uv_Off_right > UV_OFF_HIGH Or Uv_Off_right < UV_OFF_LOW Then
                Test = False
                Grid1.Item(1, 1).Style.ForeColor = Color.Red
            Else
                Grid1.Item(1, 1).Style.ForeColor = Color.Green
            End If
        End If

        Grid1.ClearSelection()
        Ridimensiona_Griglia(Grid1)

        Grid1.Visible = True

        Application.DoEvents()

        If Test = False Then
            Stringa_Errore = "Test offset elettrico Fallito!" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"
            Test_UV = False
            Errore_UV = "Fail | " & Uv_Off_left '& " | " & Uv_Off_right & " | "
            Exit Function
        End If

        Scrivi("Test offset elettrico UV Superato", Color.Green)
        Debug_test_control()
        Grid1.Visible = False
        '1.Upload della pagina di Spt che contiene le correnti native:
        'D3 06 06 03   128 x [0d 0d] – System Spt page #3

        '0x0C-0x0D UV_LEFT_NAT_IGEN(RS2x)
        '0x0E-0x0F UV_RIGHT_NAT_IGEN(RS2x)

        '2.Upload del vettore delle correnti usando la richiesta l’istanza del comando Upload D3 06 33  16 x [0d 0d]:

        '        Address()
        '        Meaning()
        '0x00-0x01 UV_LEFT_IGEN(RS2x)
        '0x02-0x03 UV_RIGHT_IGEN(RS2x)

        '3.A questo punto verificare che i valori attuali delle correnti, nel confronto con i valori nativi, siano rispettosi delle tolleranze espresse nel file INI.

        Scrivi("Upload SPT in corso....", Color.Black)

        traspsend("D3060603", 256, 8000)
        aa = stringa_ricezione(Traspric)
        Dim UV_LEFT_NAT_IGEN As Integer
        Dim UV_RIGHT_NAT_IGEN As Integer

        UV_LEFT_NAT_IGEN = "&h" & Mid$(TOGLIE_0(aa), 25, 4)
        UV_RIGHT_NAT_IGEN = "&h" & Mid$(TOGLIE_0(aa), 29, 4)

        'UV_LEFT_IGEN_TOLERANCE As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_LEFT_IGEN_TOLERANCE")
        'UV_RIGHT_IGEN_TOLERANCE As Integer = Read_ParPiastra("Sensore UV attrezzo collaudo", "UV_RIGHT_IGEN_TOLERANCE")

        Traspsend("D30633", 32, 6000)

        aa = stringa_ricezione(Traspric)
        Dim UV_LEFT_IGEN As Integer
        Dim UV_RIGHT_IGEN As Integer

        UV_LEFT_IGEN = "&h" & Mid$(TOGLIE_0(aa), 1, 4)
        UV_RIGHT_IGEN = "&h" & Mid$(TOGLIE_0(aa), 5, 4)

        Grid1.Item(0, 0).Value = "UV Corrente Left"
        Grid1.Item(1, 0).Value = UV_LEFT_IGEN
        Grid1.Item(2, 0).Value = "[" & UV_LEFT_NAT_IGEN - UV_LEFT_IGEN_TOLERANCE & "-" & UV_LEFT_NAT_IGEN + UV_LEFT_IGEN_TOLERANCE & "]"

        If UV_LEFT_IGEN < UV_LEFT_NAT_IGEN - UV_LEFT_IGEN_TOLERANCE Or UV_LEFT_IGEN > UV_LEFT_NAT_IGEN + UV_LEFT_IGEN_TOLERANCE Then
            Test = False
            Grid1.Item(1, 0).Style.ForeColor = Color.Red
        Else
            Grid1.Item(1, 0).Style.ForeColor = Color.Green
        End If
        If UVSoloCanaleSx = False Then
            Grid1.Item(0, 1).Value = "UV Corrente Right"
            Grid1.Item(1, 1).Value = UV_RIGHT_IGEN
            Grid1.Item(2, 1).Value = "[" & UV_RIGHT_NAT_IGEN - UV_RIGHT_IGEN_TOLERANCE & "-" & UV_RIGHT_NAT_IGEN + UV_RIGHT_IGEN_TOLERANCE & "]"
            If UV_RIGHT_IGEN < UV_RIGHT_NAT_IGEN - UV_RIGHT_IGEN_TOLERANCE Or UV_RIGHT_IGEN > UV_RIGHT_NAT_IGEN + UV_RIGHT_IGEN_TOLERANCE Then
                Test = False
                Grid1.Item(1, 1).Style.ForeColor = Color.Red
            Else
                Grid1.Item(1, 1).Style.ForeColor = Color.Green
            End If
        End If

        Ridimensiona_Griglia(Grid1)

        Grid1.Visible = True
        Application.DoEvents()

        If Test = False Then
            Test_UV = False
            Stringa_Errore = ("Test Correnti Fallito!" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire")
            Errore_UV = "Fail | " & Uv_Off_left & " | " & Uv_Off_right & " | " & UV_LEFT_IGEN & " | " & UV_RIGHT_IGEN & " | "
            Exit Function
        End If

        Scrivi("Test Correnti UV Superato", Color.Green)
        Debug_test_control()
        Grid1.Visible = False
        '1.A questo punto eseguire l’upload dei pattern relativi ai canali LEFT e RIGHT  con le seguenti richieste:
        'D3 06 0D  128 x [0d 0d] - lettura pattern canale LEFT;
        'D3 06 1B  128 x [0d 0d] - lettura pattern canale RIGHT;

        'Per ciascuno dei due pattern ricercare i valori minimo, medio e massimo e confrontarli con i riferimenti 
        'contenuti(nell) 'INI file per ciascun canale secondo la seguente logica: Min >= RifeMin, RifeMedLo < Med <RifeMedHi, 
        'Max <= RifeMax). Se queste condizioni sono verificate allora l’elettronica UV è da considerarsi funzionante.
        Scrivi("Acquisizione pattern Left in corso....", Color.Black)
        Dim Array_LEFT As String
        Dim Array_RIGHT As String
        Dim min_Left As Integer = 255
        Dim max_Left As Integer = 0
        Dim med_Left As Single = 0
        Dim min_Right As Integer = 255
        Dim max_Right As Integer = 0
        Dim med_Right As Single = 0
        Traspsend("D30D", 3, 8000)
        Traspsend("D30D", 3, 8000)
        Traspsend("D30D", 3, 8000)

        traspsend("D3060D", 256, 8000)
        Array_LEFT = TOGLIE_0(stringa_ricezione(Traspric))

        For a = 1 To 200 Step 2
            aa = CInt("&h" & Mid$(Array_LEFT, a, 2))
            If aa < min_Left Then min_Left = aa
            If aa > max_Left Then max_Left = aa
            med_Left = med_Left + aa
        Next
        med_Left = med_Left / 100
        Grid1.RowCount = 3
        Grid1.Item(0, 0).Value = "Uv pattern Min_Left"
        Grid1.Item(0, 1).Value = "Uv Pattern Med_Left"
        Grid1.Item(0, 2).Value = "Uv Pattern Max_Left"
        Grid1.Item(1, 0).Value = min_Left
        Grid1.Item(1, 1).Value = med_Left
        Grid1.Item(1, 2).Value = max_Left
        Grid1.Item(2, 0).Value = "[>=" & UV_LEFT_RIFE_MIN & "]"
        Grid1.Item(2, 1).Value = "[" & UV_LEFT_RIFE_MED_LO & "-" & UV_LEFT_RIFE_MED_HI & "]"
        Grid1.Item(2, 2).Value = "[<=" & UV_LEFT_RIFE_MAX & "]"
        If min_Left < UV_LEFT_RIFE_MIN Then
            Test = False
            Grid1.Item(1, 0).Style.ForeColor = Color.Red
        Else
            Grid1.Item(1, 0).Style.ForeColor = Color.Green
        End If
        If med_Left < UV_LEFT_RIFE_MED_LO Or med_Left > UV_LEFT_RIFE_MED_HI Then
            Test = False
            Grid1.Item(1, 1).Style.ForeColor = Color.Red
        Else
            Grid1.Item(1, 1).Style.ForeColor = Color.Green
        End If
        If max_Left > UV_LEFT_RIFE_MAX Then
            Test = False
            Grid1.Item(1, 2).Style.ForeColor = Color.Red
        Else
            Grid1.Item(1, 2).Style.ForeColor = Color.Green
        End If

        If UVSoloCanaleSx = False Then
            Grid1.RowCount = 6
            Scrivi("Acquisizione pattern Right in corso....", Color.Black)
            Traspsend("D3061B", 256, 8000)
            Array_RIGHT = TOGLIE_0(stringa_ricezione(Traspric))

            For a = 1 To 200 Step 2
                aa = CInt("&h" & Mid$(Array_RIGHT, a, 2))
                If aa < min_Right Then min_Right = aa
                If aa > max_Right Then max_Right = aa
                med_Right = med_Right + aa
            Next
            med_Right = med_Right / 100
            Grid1.Item(0, 3).Value = "Uv pattern Min_Right"
            Grid1.Item(0, 4).Value = "Uv Pattern Med_Right"
            Grid1.Item(0, 5).Value = "Uv Pattern Max_Right"
            Grid1.Item(1, 3).Value = min_Right
            Grid1.Item(1, 4).Value = med_Right
            Grid1.Item(1, 5).Value = max_Right
            Grid1.Item(2, 3).Value = "[>=" & UV_RIGHT_RIFE_MIN & "]"
            Grid1.Item(2, 4).Value = "[" & UV_RIGHT_RIFE_MED_LO & "-" & UV_RIGHT_RIFE_MED_HI & "]"
            Grid1.Item(2, 5).Value = "[<=" & UV_RIGHT_RIFE_MAX & "]"
            If min_Right < UV_RIGHT_RIFE_MIN Then
                Test = False
                Grid1.Item(1, 3).Style.ForeColor = Color.Red
            Else
                Grid1.Item(1, 3).Style.ForeColor = Color.Green
            End If

            If med_Right < UV_RIGHT_RIFE_MED_LO Or med_Right > UV_RIGHT_RIFE_MED_HI Then
                Test = False
                Grid1.Item(1, 4).Style.ForeColor = Color.Red
            Else
                Grid1.Item(1, 4).Style.ForeColor = Color.Green
            End If

            If max_Right > UV_RIGHT_RIFE_MAX Then
                Test = False
                Grid1.Item(1, 5).Style.ForeColor = Color.Red
            Else
                Grid1.Item(1, 5).Style.ForeColor = Color.Green
            End If
        End If
        Ridimensiona_Griglia(Grid1)


        Grid1.Visible = True
        Application.DoEvents()

        If Test = False Then
            Stringa_Errore = "Test pattern Fallito!" & Chr(13) & "Premere un tasto per ripetere, 'Esc' per uscire"
            Errore_UV = "Fail | " & Uv_Off_left & " | " & Uv_Off_right & " | " & UV_LEFT_IGEN & " | " & UV_RIGHT_IGEN & " | " & min_Left & " | " & med_Left & " | " & max_Left & " | "
            If UVSoloCanaleSx = False Then Errore_UV = "Fail | " & Uv_Off_left & " | " & Uv_Off_right & " | " & UV_LEFT_IGEN & " | " & UV_RIGHT_IGEN & " | " & min_Left & " | " & med_Left & " | " & max_Left & " | " & min_Right & " | " & med_Right & " | " & max_Right & " | "
            Test_UV = False
            Exit Function
        End If
        Stringa_Salvataggio = Stringa_Salvataggio & "Pass | " & Uv_Off_left & " | " & Uv_Off_right & " | " & UV_LEFT_IGEN & " | " & UV_RIGHT_IGEN & " | " & min_Left & " | " & med_Left & " | " & max_Left & " | "
        If UVSoloCanaleSx = False Then Stringa_Salvataggio = Stringa_Salvataggio & "Pass | " & Uv_Off_left & " | " & Uv_Off_right & " | " & UV_LEFT_IGEN & " | " & UV_RIGHT_IGEN & " | " & min_Left & " | " & med_Left & " | " & max_Left & " | " & min_Right & " | " & med_Right & " | " & max_Right & " | "


        Scrivi("Test pattern UV Superato", Color.Green)

        Debug_test_control()

        Test_UV = True

    End Function

    Public Function Test_Tape_Mag() As Boolean
        'Per quanto riguarda il testing della connessione con i moduli periferici, si deve eseguire il 
        'cmd SystemChecklist (D3 03  StatusCode): se lo StatusCode è READY (0x40) allora si può ritenere funzionante 
        'il link verso i dispositivi MAG e TAPE sensor. 
        'Se lo stato non dovesse risultare READY, allora usare il comando di upload della 
        'DevicesStatusString (Upload DSS – D3 06 0B  8 * [0d 0d]) per verificare se effettivamente 
        'uno dei due link non funziona

        Dim aa As String
        Dim Mag As String
        Dim Tape As String
        Test_Tape_Mag = False
        Scrivi("Preset in corso...", Color.Black)
        Call traspsend("D303", 1, 6000)
        If stringa_ricezione(Traspric) <> "40" Then
            Call traspsend("D3060B", 16, 5000)
            aa = TOGLIE_0(stringa_ricezione(Traspric))
            Mag = Mid$(aa, 13, 2)
            Tape = Mid$(aa, 9, 2)
            Mag = Mid(hex_bin(Mag), 7, 2)
            Tape = Mid(hex_bin(Tape), 3, 2)
            If Read_Set("Configurazione", "TestTape") = "1" Then
                If Tape <> "01" And Mag <> "01" Then
                    Stringa_Errore = ("Fallito test Magnetico e Tape" & Chr(13) & "Mag=" & Mag & Chr(13) & "Tape=" & Tape & Chr(13) & "Premere un tasto per continuare, ESC per uscire")
                    Errore_Tape_Mag = "Fail | Fail | "
                    Exit Function
                End If
                If Tape <> "01" Then
                    Stringa_Errore = ("Fallito test Tape" & Chr(13) & "Tape=" & Tape & Chr(13) & "Premere un tasto per continuare, ESC per uscire")
                    Errore_Tape_Mag = "Fail | Pass | "
                    Exit Function
                End If
            End If
            If Mag <> "01" Then
                    Stringa_Errore = ("Fallito test Magnetico" & Chr(13) & "Mag=" & Mag & Chr(13) & "Premere un tasto per continuare, ESC per uscire")
                    Errore_Tape_Mag = "Pass | Fail | "
                    Exit Function
                End If
            End If

            Scrivi("Verifica comunicazione con moduli periferici di TAPE e MAG Superato", Color.Green)
        Test_Tape_Mag = True
        Stringa_Salvataggio = Stringa_Salvataggio & "Pass | Pass | "

    End Function

    Private Sub Debug_test_control()

        If Debug_pausa > 0 Then
            MySleep(Debug_pausa * 1000)
        End If

        If Debug_test = True Then
            Scrivi("Pausa di debug, premi un tasto per continuare", Color.Black)
            Attendi_Tasto()
        End If
    End Sub

    Public Sub Ridimensiona_Griglia(ByVal Griglia As Object)

        Dim Larghezza As Integer
        Dim Altezza As Integer

        For a = 1 To Grid1.Columns.Count
            Grid1.AutoResizeColumn(a - 1)
        Next

        Larghezza = 3
        For a = 1 To Grid1.Columns.Count
            Larghezza = Larghezza + Griglia.Columns(a - 1).Width
        Next
        Griglia.Width = Larghezza

        Altezza = 3
        For a = 1 To Griglia.Rows.Count
            Altezza = Altezza + Griglia.Rows(a - 1).Height
        Next

        Griglia.Height = Altezza
        Griglia.Top = Screen.PrimaryScreen.Bounds.Height / 5
        Griglia.Left = (Screen.PrimaryScreen.Bounds.Width - Griglia.Width) / 2
        Grid1.Refresh()
        Application.DoEvents()
    End Sub
    Public Sub cancella_griglia(ByVal Griglia As Object)
        For a = 0 To Griglia.Rows.Count - 1
            For b = 0 To Griglia.Columns.Count - 1
                Griglia.Item(b, a).Style.ForeColor = Color.Black
                Griglia.Item(b, a).Value = ""
            Next
        Next
    End Sub

    Public Sub Scrivi(ByVal Stringa As String, ByVal col As Color)
        Label1.ForeColor = col
        Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Function Test_Cis() As Boolean

        Dim OFF_MAX_DELTA As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "OFF_MAX_DELTA")
        Dim PWM_MAX_DELTA As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "PWM_MAX_DELTA")
        Dim IGEN_MAX_DELTA As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "IGEN_MAX_DELTA")
        Dim OPT_WHITE_PATTERN_DELTA_PERC As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "OPT_WHITE_PATTERN_DELTA_PERC")
        Dim OPT_DARK_PATTERN_DELTA_PERC As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "OPT_DARK_PATTERN_DELTA_PERC")
        Dim Numero_canali As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "numero_canali")

        If Numero_canali = 3 Then

            Dim Test As Boolean = True

            Scrivi("Preset in corso....", Color.Black)
            Traspsend("83", 1, 10000)

            Scrivi("Caricamento SPT 0....", Color.Black)
            Traspsend("D3060600", 256, 10000)
            Dim spt0 = TOGLIE_0(stringa_ricezione(Traspric))

            Scrivi("Caricamento SPT 1....", Color.Black)
            Traspsend("D3060601", 256, 10000)
            Dim spt1 = TOGLIE_0(stringa_ricezione(Traspric))

            Dim stato_sensori = Mid(spt0, 19, 2)

            cancella_griglia(Grid1)

            Grid1.RowCount = 2
            Grid1.ColumnCount = 8

            Grid1.Item(0, 0).Value = "Ir Rear"
            Grid1.Item(1, 0).Value = "Rd Rear"
            Grid1.Item(2, 0).Value = "Gr Rear"
            Grid1.Item(3, 0).Value = "Ir Front"
            Grid1.Item(4, 0).Value = "Rd Front"
            Grid1.Item(5, 0).Value = "Gr Front"
            Grid1.Item(6, 0).Value = "Dark Rear"
            Grid1.Item(7, 0).Value = "Dark Front"

            Dim stato_sens_bin = hex_bin(stato_sensori)

            Grid1.Item(0, 1).Value = Mid(stato_sens_bin, 1, 1)
            Grid1.Item(1, 1).Value = Mid(stato_sens_bin, 2, 1)
            Grid1.Item(2, 1).Value = Mid(stato_sens_bin, 3, 1)
            Grid1.Item(3, 1).Value = Mid(stato_sens_bin, 4, 1)
            Grid1.Item(4, 1).Value = Mid(stato_sens_bin, 5, 1)
            Grid1.Item(5, 1).Value = Mid(stato_sens_bin, 6, 1)
            Grid1.Item(6, 1).Value = Mid(stato_sens_bin, 7, 1)
            Grid1.Item(7, 1).Value = Mid(stato_sens_bin, 8, 1)

            For a = 0 To 7
                If Grid1(a, 1).Value = 1 Then
                    Grid1.Item(a, 1).Style.ForeColor = Color.Red
                Else
                    Grid1.Item(a, 1).Style.ForeColor = Color.Green
                End If
            Next

            Ridimensiona_Griglia(Grid1)

            Grid1.Visible = True

            Application.DoEvents()

            If stato_sensori <> "00" Then
                Test_Cis = False
                Errore_Cis = " False " & " | " & stato_sens_bin & " | "
                Stringa_Errore = "Fallito stato sensori, premere un tasto per ripere, Esc per uscire"
                Exit Function
            End If

            Scrivi("Verifica stato sensori Superata", Color.Green)

            Debug_test_control()

            'Front CIS Offset (seg 1)[01:0x0000][01:0x0006]
            'Front CIS Offset (seg 2)[01:0x0001][01:0x0007]
            'Front CIS Offset (seg 3)[01:0x0002][01:0x0008]
            'Rear CIS Offset (seg 1)[01:0x0003][01:0x0009]
            'Rear CIS Offset (seg 2)[01:0x0004][01:0x000A]
            'Rear CIS Offset (seg 3)[01:0x0005][01:0x000B]
            'Front CIS GR Igen[00:0x0018][00:0x0000]
            'Front CIS IR Igen[00:0x001A][00:0x0002]
            'Front CIS RD Igen[00:0x002C][00:0x0024]
            'Rear CIS GR Igen[00:0x001C][00:0x0008]
            'Rear CIS IR Igen[00:0x001E][00:0x000A]
            'Rear CIS RD Igen[00:0x002E][00:0x0026]
            'Front CIS GR Pwm[00:0x0019][00:0x0001]
            'Front CIS IR Pwm[00:0x001B][00:0x0003]
            'Front CIS RD Pwm[00:0x002D][00:0x0025]
            'Rear CIS GR Pwm[00:0x001D][00:0x0009]
            'Rear CIS IR Pwm[00:0x001F][00:0x000B]
            'Rear CIS RD Pwm[00:0x002F][00:0x0027]

            cancella_griglia(Grid1)
            Grid1.RowCount = 18
            Grid1.ColumnCount = 3

            Grid1.Item(0, 0).Value = "Front CIS Offset (seg 1)"
            Grid1.Item(0, 1).Value = "Front CIS Offset (seg 2)"
            Grid1.Item(0, 2).Value = "Front CIS Offset (seg 3)"
            Grid1.Item(0, 3).Value = "Rear CIS Offset (seg 1)"
            Grid1.Item(0, 4).Value = "Rear CIS Offset (seg 2)"
            Grid1.Item(0, 5).Value = "Rear CIS Offset (seg 3)"


            Dim Front_CIS_Offset(2, 3) As Integer
            Dim Rear_CIS_Offset(2, 3) As Integer
            'allo 0 stanno i nativi, all'1 stanno gli attuali
            Front_CIS_Offset(0, 0) = Val("&H" + Mid(spt1, 1, 4))
            Front_CIS_Offset(0, 1) = Val("&H" + Mid(spt1, 5, 4))
            Front_CIS_Offset(0, 2) = Val("&H" + Mid(spt1, 9, 4))

            Rear_CIS_Offset(0, 0) = Val("&H" + Mid(spt1, 13, 4))
            Rear_CIS_Offset(0, 1) = Val("&H" + Mid(spt1, 17, 4))
            Rear_CIS_Offset(0, 2) = Val("&H" + Mid(spt1, 21, 4))

            Front_CIS_Offset(1, 0) = Val("&H" + Mid(spt1, 25, 4))
            Front_CIS_Offset(1, 1) = Val("&H" + Mid(spt1, 29, 4))
            Front_CIS_Offset(1, 2) = Val("&H" + Mid(spt1, 33, 4))

            Rear_CIS_Offset(1, 0) = Val("&H" + Mid(spt1, 37, 4))
            Rear_CIS_Offset(1, 1) = Val("&H" + Mid(spt1, 41, 4))
            Rear_CIS_Offset(1, 2) = Val("&H" + Mid(spt1, 45, 4))

            'scrivo i valori attuali
            Grid1.Item(1, 0).Value = Front_CIS_Offset(1, 0)
            Grid1.Item(1, 1).Value = Front_CIS_Offset(1, 1)
            Grid1.Item(1, 2).Value = Front_CIS_Offset(1, 2)
            Grid1.Item(1, 3).Value = Rear_CIS_Offset(1, 0)
            Grid1.Item(1, 4).Value = Rear_CIS_Offset(1, 1)
            Grid1.Item(1, 5).Value = Rear_CIS_Offset(1, 2)

            'scrivo le soglie
            Grid1.Item(2, 0).Value = "[" & Front_CIS_Offset(0, 0) - OFF_MAX_DELTA & "/" & Front_CIS_Offset(0, 0) + OFF_MAX_DELTA & "]"
            Grid1.Item(2, 1).Value = "[" & Front_CIS_Offset(0, 1) - OFF_MAX_DELTA & "/" & Front_CIS_Offset(0, 1) + OFF_MAX_DELTA & "]"
            Grid1.Item(2, 2).Value = "[" & Front_CIS_Offset(0, 2) - OFF_MAX_DELTA & "/" & Front_CIS_Offset(0, 2) + OFF_MAX_DELTA & "]"
            Grid1.Item(2, 3).Value = "[" & Rear_CIS_Offset(0, 0) - OFF_MAX_DELTA & "/" & Rear_CIS_Offset(0, 0) + OFF_MAX_DELTA & "]"
            Grid1.Item(2, 4).Value = "[" & Rear_CIS_Offset(0, 1) - OFF_MAX_DELTA & "/" & Rear_CIS_Offset(0, 1) + OFF_MAX_DELTA & "]"
            Grid1.Item(2, 5).Value = "[" & Rear_CIS_Offset(0, 2) - OFF_MAX_DELTA & "/" & Rear_CIS_Offset(0, 2) + OFF_MAX_DELTA & "]"



            For a = 0 To 2
                If Front_CIS_Offset(1, a) < Front_CIS_Offset(0, a) - OFF_MAX_DELTA Or Front_CIS_Offset(1, a) > Front_CIS_Offset(0, a) + OFF_MAX_DELTA Then
                    Grid1.Item(1, a).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a).Style.ForeColor = Color.Green
                End If
            Next

            For a = 0 To 2
                If Rear_CIS_Offset(1, a) < Rear_CIS_Offset(0, a) - OFF_MAX_DELTA Or Rear_CIS_Offset(1, a) > Rear_CIS_Offset(0, a) + OFF_MAX_DELTA Then
                    Grid1.Item(1, a + 3).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 3).Style.ForeColor = Color.Green
                End If
            Next

            Grid1.Item(0, 6).Value = "Front CIS GR Igen"
            Grid1.Item(0, 7).Value = "Front CIS IR Igen"
            Grid1.Item(0, 8).Value = "Front CIS RD Igen"
            Grid1.Item(0, 9).Value = "Rear CIS GR Igen"
            Grid1.Item(0, 10).Value = "Rear CIS IR Igen"
            Grid1.Item(0, 11).Value = "Rear CIS RD Igen"
            Grid1.Item(0, 12).Value = "Front CIS GR Pwm"
            Grid1.Item(0, 13).Value = "Front CIS IR Pwm"
            Grid1.Item(0, 14).Value = "Front CIS RD Pwm"
            Grid1.Item(0, 15).Value = "Rear CIS GR Pwm"
            Grid1.Item(0, 16).Value = "Rear CIS IR Pwm"
            Grid1.Item(0, 17).Value = "Rear CIS RD Pwm"

            Dim Front_CIS_Igen(2, 3) As Integer
            Dim Rear_CIS_Igen(2, 3) As Integer
            'allo 0 stanno i nativi, all'1 stanno gli attuali Gr Ir RD
            Front_CIS_Igen(0, 0) = Val("&H" + Mid$(spt0, 97, 4))
            Front_CIS_Igen(0, 1) = Val("&H" + Mid$(spt0, 105, 4))
            Front_CIS_Igen(0, 2) = Val("&H" + Mid$(spt0, 177, 4))

            Rear_CIS_Igen(0, 0) = Val("&H" + Mid$(spt0, 113, 4))
            Rear_CIS_Igen(0, 1) = Val("&H" + Mid$(spt0, 121, 4))
            Rear_CIS_Igen(0, 2) = Val("&H" + Mid$(spt0, 185, 4))

            Front_CIS_Igen(1, 0) = Val("&H" + Mid$(spt0, 1, 4))
            Front_CIS_Igen(1, 1) = Val("&H" + Mid$(spt0, 9, 4))
            Front_CIS_Igen(1, 2) = Val("&H" + Mid$(spt0, 145, 4))

            Rear_CIS_Igen(1, 0) = Val("&H" + Mid$(spt0, 33, 4))
            Rear_CIS_Igen(1, 1) = Val("&H" + Mid$(spt0, 41, 4))
            Rear_CIS_Igen(1, 2) = Val("&H" + Mid$(spt0, 153, 4))

            'scrivo i valori attuali
            Grid1.Item(1, 6).Value = Front_CIS_Igen(1, 0)
            Grid1.Item(1, 7).Value = Front_CIS_Igen(1, 1)
            Grid1.Item(1, 8).Value = Front_CIS_Igen(1, 2)
            Grid1.Item(1, 9).Value = Rear_CIS_Igen(1, 0)
            Grid1.Item(1, 10).Value = Rear_CIS_Igen(1, 1)
            Grid1.Item(1, 11).Value = Rear_CIS_Igen(1, 2)


            'scrivo le soglie
            Grid1.Item(2, 6).Value = "[" & Front_CIS_Igen(0, 0) - IGEN_MAX_DELTA & "/" & Front_CIS_Igen(0, 0) + IGEN_MAX_DELTA & "]"
            Grid1.Item(2, 7).Value = "[" & Front_CIS_Igen(0, 1) - IGEN_MAX_DELTA & "/" & Front_CIS_Igen(0, 1) + IGEN_MAX_DELTA & "]"
            Grid1.Item(2, 8).Value = "[" & Front_CIS_Igen(0, 2) - IGEN_MAX_DELTA & "/" & Front_CIS_Igen(0, 2) + IGEN_MAX_DELTA & "]"
            Grid1.Item(2, 9).Value = "[" & Rear_CIS_Igen(0, 0) - IGEN_MAX_DELTA & "/" & Rear_CIS_Igen(0, 0) + IGEN_MAX_DELTA & "]"
            Grid1.Item(2, 10).Value = "[" & Rear_CIS_Igen(0, 1) - IGEN_MAX_DELTA & "/" & Rear_CIS_Igen(0, 1) + IGEN_MAX_DELTA & "]"
            Grid1.Item(2, 11).Value = "[" & Rear_CIS_Igen(0, 2) - IGEN_MAX_DELTA & "/" & Rear_CIS_Igen(0, 2) + IGEN_MAX_DELTA & "]"



            For a = 0 To 2
                If Front_CIS_Igen(1, a) < Front_CIS_Igen(0, a) - IGEN_MAX_DELTA Or Front_CIS_Igen(1, a) > Front_CIS_Igen(0, a) + IGEN_MAX_DELTA Then
                    Grid1.Item(1, a + 6).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 6).Style.ForeColor = Color.Green
                End If
            Next

            For a = 0 To 2
                If Rear_CIS_Igen(1, a) < Rear_CIS_Igen(0, a) - IGEN_MAX_DELTA Or Rear_CIS_Igen(1, a) > Rear_CIS_Igen(0, a) + IGEN_MAX_DELTA Then
                    Grid1.Item(1, a + 9).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 9).Style.ForeColor = Color.Green
                End If
            Next

            Grid1.Item(0, 12).Value = "Front CIS GR Pwm"
            Grid1.Item(0, 13).Value = "Front CIS IR Pwm"
            Grid1.Item(0, 14).Value = "Front CIS RD Pwm"
            Grid1.Item(0, 15).Value = "Rear CIS GR Pwm"
            Grid1.Item(0, 16).Value = "Rear CIS IR Pwm"
            Grid1.Item(0, 17).Value = "Rear CIS RD Pwm"

            Dim Front_CIS_Pwm(2, 3) As Integer
            Dim Rear_CIS_Pwm(2, 3) As Integer
            'allo 0 stanno i nativi, all'1 stanno gli attuali Gr Ir RD
            Front_CIS_Pwm(0, 0) = Val("&H" + Mid$(spt0, 101, 4))
            Front_CIS_Pwm(0, 1) = Val("&H" + Mid$(spt0, 109, 4))
            Front_CIS_Pwm(0, 2) = Val("&H" + Mid$(spt0, 181, 4))

            Rear_CIS_Pwm(0, 0) = Val("&H" + Mid$(spt0, 117, 4))
            Rear_CIS_Pwm(0, 1) = Val("&H" + Mid$(spt0, 125, 4))
            Rear_CIS_Pwm(0, 2) = Val("&H" + Mid$(spt0, 189, 4))

            Front_CIS_Pwm(1, 0) = Val("&H" + Mid$(spt0, 5, 4))
            Front_CIS_Pwm(1, 1) = Val("&H" + Mid$(spt0, 13, 4))
            Front_CIS_Pwm(1, 2) = Val("&H" + Mid$(spt0, 149, 4))

            Rear_CIS_Pwm(1, 0) = Val("&H" + Mid$(spt0, 37, 4))
            Rear_CIS_Pwm(1, 1) = Val("&H" + Mid$(spt0, 45, 4))
            Rear_CIS_Pwm(1, 2) = Val("&H" + Mid$(spt0, 157, 4))

            'scrivo i valori attuali
            Grid1.Item(1, 12).Value = Front_CIS_Pwm(1, 0)
            Grid1.Item(1, 13).Value = Front_CIS_Pwm(1, 1)
            Grid1.Item(1, 14).Value = Front_CIS_Pwm(1, 2)
            Grid1.Item(1, 15).Value = Rear_CIS_Pwm(1, 0)
            Grid1.Item(1, 16).Value = Rear_CIS_Pwm(1, 1)
            Grid1.Item(1, 17).Value = Rear_CIS_Pwm(1, 2)


            'scrivo le soglie
            Grid1.Item(2, 12).Value = "[" & Front_CIS_Pwm(0, 0) - PWM_MAX_DELTA & "/" & Front_CIS_Pwm(0, 0) + PWM_MAX_DELTA & "]"
            Grid1.Item(2, 13).Value = "[" & Front_CIS_Pwm(0, 1) - PWM_MAX_DELTA & "/" & Front_CIS_Pwm(0, 1) + PWM_MAX_DELTA & "]"
            Grid1.Item(2, 14).Value = "[" & Front_CIS_Pwm(0, 2) - PWM_MAX_DELTA & "/" & Front_CIS_Pwm(0, 2) + PWM_MAX_DELTA & "]"
            Grid1.Item(2, 15).Value = "[" & Rear_CIS_Pwm(0, 0) - PWM_MAX_DELTA & "/" & Rear_CIS_Pwm(0, 0) + PWM_MAX_DELTA & "]"
            Grid1.Item(2, 16).Value = "[" & Rear_CIS_Pwm(0, 1) - PWM_MAX_DELTA & "/" & Rear_CIS_Pwm(0, 1) + PWM_MAX_DELTA & "]"
            Grid1.Item(2, 17).Value = "[" & Rear_CIS_Pwm(0, 2) - PWM_MAX_DELTA & "/" & Rear_CIS_Pwm(0, 2) + PWM_MAX_DELTA & "]"

            For a = 0 To 2
                If Front_CIS_Pwm(1, a) < Front_CIS_Pwm(0, a) - PWM_MAX_DELTA Or Front_CIS_Pwm(1, a) > Front_CIS_Pwm(0, a) + PWM_MAX_DELTA Then
                    Grid1.Item(1, a + 12).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 12).Style.ForeColor = Color.Green
                End If
            Next

            For a = 0 To 2
                If Rear_CIS_Pwm(1, a) < Rear_CIS_Pwm(0, a) - PWM_MAX_DELTA Or Rear_CIS_Pwm(1, a) > Rear_CIS_Pwm(0, a) + PWM_MAX_DELTA Then
                    Grid1.Item(1, a + 15).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 15).Style.ForeColor = Color.Green
                End If
            Next

            If Test = False Then
                Grid1.Item(1, 0).Value = Front_CIS_Offset(1, 0)
                Grid1.Item(1, 1).Value = Front_CIS_Offset(1, 1)
                Grid1.Item(1, 2).Value = Front_CIS_Offset(1, 2)
                Grid1.Item(1, 3).Value = Rear_CIS_Offset(1, 0)
                Grid1.Item(1, 4).Value = Rear_CIS_Offset(1, 1)
                Grid1.Item(1, 5).Value = Rear_CIS_Offset(1, 2)

                Errore_Cis = " False " & " | " & stato_sens_bin & " | " & Front_CIS_Offset(1, 0) & " | " & Front_CIS_Offset(1, 1) & " | " & Front_CIS_Offset(1, 2) & " | " & Rear_CIS_Offset(1, 0) & " | " & Rear_CIS_Offset(1, 1) & " | " & Rear_CIS_Offset(1, 2) & " | "
                Errore_Cis = Errore_Cis & Front_CIS_Igen(1, 0) & " | " & Front_CIS_Igen(1, 1) & " | " & Front_CIS_Igen(1, 2) & " | " & Rear_CIS_Igen(1, 0) & " | " & Rear_CIS_Igen(1, 1) & " | " & Rear_CIS_Igen(1, 2) & " | "
                Errore_Cis = Errore_Cis & Front_CIS_Pwm(1, 0) & " | " & Front_CIS_Pwm(1, 1) & " | " & Front_CIS_Pwm(1, 2) & " | " & Rear_CIS_Pwm(1, 0) & " | " & Rear_CIS_Pwm(1, 1) & " | " & Rear_CIS_Pwm(1, 2)

                Ridimensiona_Griglia(Grid1)
                Grid1.Visible = True
                Test_Cis = False
                Stringa_Errore = "Fallita verifica cis, premere un tasto per ripere, Esc per uscire"
                Exit Function
            End If

            Traspsend("83", 1, 10000)

            Test_Cis = True
            Ridimensiona_Griglia(Grid1)
            Grid1.Visible = True
            Application.DoEvents()
            Debug_test_control()
            Grid1.Visible = False

            cancella_griglia(Grid1)
            Grid1.RowCount = 5
            Grid1.ColumnCount = 3
            Grid1.Item(0, 1).Value = "Scanline Dark"
            Grid1.Item(0, 2).Value = "Scanline Gr"
            Grid1.Item(0, 3).Value = "Scanline Ir"
            Grid1.Item(0, 4).Value = "Scanline Rd"
            Grid1.Item(1, 0).Value = " Front "
            Grid1.Item(2, 0).Value = " Rear "
            Ridimensiona_Griglia(Grid1)

            'Mem_Dump(1, "005F0000", "00000A00")
            'Dim Cis_preset(2, 4)
            'Dim pattern_preset As String = stringa_ricezione(Traspric)

            Dim Cis_preset(2, 4, 256) As Byte
            'Dim pattern_preset() As Byte = USBB("301F", 2, 65536, True)
            Dim pattern_preset() As Byte = USBB("301F", 65536, 65536, True)



            'pattern_preset = stringa_ricezione(pattern_preset)




            '--  256:
            'Rear Dark Pattern 256:
            '--256:
            'Front GR Pattern 256:
            'Front IR Pattern 256:
            'Front RD Pattern 256:
            'Rear GR Pattern 256:
            'Rear IR Pattern 256:
            'Rear RD Pattern 256:
            '-- 62976:

            ' 0=front 1=rear - 0=dark, 1=gr 2=ir 3=rd
            'Buffer.BlockCopy(pattern_preset, 0, Cis_preset(0, 0), 0, 256)
            For a = 0 To 255
                Cis_preset(0, 0, a) = pattern_preset(a)
                Cis_preset(1, 0, a) = pattern_preset(a + 512) '= stringa_ricezione(Mid(pattern_preset, 513, 256))
                Cis_preset(0, 1, a) = pattern_preset(a + 1024) 'stringa_ricezione(Mid(pattern_preset, 1025, 256))
                Cis_preset(0, 2, a) = pattern_preset(a + 1280) 'stringa_ricezione(Mid(pattern_preset, 1281, 256))
                Cis_preset(0, 3, a) = pattern_preset(a + 1536) 'stringa_ricezione(Mid(pattern_preset, 1537, 256))
                Cis_preset(1, 1, a) = pattern_preset(a + 1792) 'stringa_ricezione(Mid(pattern_preset, 1793, 256))
                Cis_preset(1, 2, a) = pattern_preset(a + 2048) 'stringa_ricezione(Mid(pattern_preset, 2049, 256))
                Cis_preset(1, 3, a) = pattern_preset(a + 2304) 'stringa_ricezione(Mid(pattern_preset, 2305, 256))
            Next
            'Cis_preset(0, 0) = stringa_ricezione(Mid(pattern_preset, 1, 256))
            'Cis_Preset(1, 0) = stringa_ricezione(Mid(pattern_Preset, 513, 256))
            'Cis_preset(0, 1) = stringa_ricezione(Mid(pattern_preset, 1025, 256))
            'Cis_Preset(0, 2) = stringa_ricezione(Mid(pattern_Preset, 1281, 256))
            'Cis_Preset(0, 3) = stringa_ricezione(Mid(pattern_Preset, 1537, 256))
            'Cis_Preset(1, 1) = stringa_ricezione(Mid(pattern_Preset, 1793, 256))
            'Cis_Preset(1, 2) = stringa_ricezione(Mid(pattern_Preset, 2049, 256))
            'Cis_Preset(1, 3) = stringa_ricezione(Mid(pattern_Preset, 2305, 256))

            'BLACK
            Dim Cis_nativi(2, 4, 256)
            'Dim Pattern_nativi() As Byte = USBB("5000", 2, 1024, False)
            Dim Pattern_nativi() As Byte = USBB("5000", 1024, 1024, False)
            ' 0=front 1=rear - 0=dark, 1=gr 2=ir 3=rd
            'Mem_Dump(9, "00320000", "00000200")
            'Dim AA As String = TOGLIE_0(stringa_ricezione(Traspric))
            'Mem_Dump(9, "00321000", "00000200")
            'AA = AA & TOGLIE_0(stringa_ricezione(Traspric))

            For a = 0 To 255
                Cis_nativi(0, 0, a) = Pattern_nativi(a)
                Cis_nativi(1, 0, a) = Pattern_nativi(a + 512)
                'Cis_nativi(0, 0) = Mid$(AA, 1, 512)
                'Cis_nativi(1, 0) = Mid$(AA, 511, 512)
            Next
            'GREEN
            'Pattern_nativi = USBB("5001", 2, 1024, False)
            Pattern_nativi = USBB("5001", 1024, 1024, False)
            For a = 0 To 255
                Cis_nativi(0, 1, a) = Pattern_nativi(a)
                Cis_nativi(1, 1, a) = Pattern_nativi(a + 512)
            Next
            'Mem_Dump(9, "00324000", "00000200")
            'AA = TOGLIE_0(stringa_ricezione(Traspric))
            'Mem_Dump(9, "00325000", "00000200")
            'AA = AA & TOGLIE_0(stringa_ricezione(Traspric))

            'Cis_nativi(0, 1) = Mid$(AA, 1, 512)
            'Cis_nativi(1, 1) = Mid$(AA, 1025, 512)

            ''IR
            'Pattern_nativi = USBB("5003", 2, 1024, False)
            Pattern_nativi = USBB("5003", 1024, 1024, False)
            For a = 0 To 255
                Cis_nativi(0, 2, a) = Pattern_nativi(a)
                Cis_nativi(1, 2, a) = Pattern_nativi(a + 512)
            Next
            'Mem_Dump(9, "00324400", "00000200")
            'AA = TOGLIE_0(stringa_ricezione(Traspric))
            'Mem_Dump(9, "00325400", "00000200")
            'AA = AA & TOGLIE_0(stringa_ricezione(Traspric))

            'Cis_nativi(0, 2) = Mid$(AA, 1, 512)
            'Cis_nativi(1, 2) = Mid$(AA, 1025, 512)

            ''RD
            'Pattern_nativi = USBB("5002", 2, 1024, False)
            Pattern_nativi = USBB("5002", 1024, 1024, False)
            For a = 0 To 255
                Cis_nativi(0, 3, a) = Pattern_nativi(a)
                Cis_nativi(1, 3, a) = Pattern_nativi(a + 512)
            Next
            'Mem_Dump(9, "00324800", "00000200")
            'AA = TOGLIE_0(stringa_ricezione(Traspric))
            'Mem_Dump(9, "00325800", "00000200")
            'AA = AA & TOGLIE_0(stringa_ricezione(Traspric))

            'Cis_nativi(0, 3) = Mid$(AA, 1, 512)
            'Cis_nativi(1, 3) = Mid$(AA, 1025, 512)
            Test = True
            Stringa_Salvataggio_Pattern_Preset = ""
            Dim PATTERN As Integer

            For a = 0 To 1
                For b = 0 To 3
                    Dim Test1 As Boolean = True
                    For c = 0 To 255

                        If b = 0 Then PATTERN = OPT_DARK_PATTERN_DELTA_PERC Else PATTERN = OPT_WHITE_PATTERN_DELTA_PERC

                        'If b = 0 Then pres = Val("&h" & Mid(Cis_preset(a, b), c, 2)) Else pres = Val("&h" & Mid(Cis_preset(a, b), c, 2)) '+ Val("&h" & Mid(Cis_preset(a, 0), c, 2))
                        Dim infe As Integer = Cis_nativi(a, b, c) - (Cis_nativi(a, b, c) * PATTERN / 100)
                        Dim supe As Integer = Cis_nativi(a, b, c) + (Cis_nativi(a, b, c) * PATTERN / 100)
                        If Cis_preset(a, b, c) < infe Or Cis_preset(a, b, c) > supe Then

                            Test = False
                            Test1 = False
                        End If
                        Stringa_Salvataggio_Pattern_Preset = Stringa_Salvataggio_Pattern_Preset & " | " & Cis_preset(a, b, c)
                    Next
                    If Test1 = True Then
                        Grid1.Item(a + 1, b + 1).Value = "Pass"
                        Grid1.Item(a + 1, b + 1).Style.ForeColor = Color.Green
                    Else
                        Grid1.Item(a + 1, b + 1).Value = "Fail"
                        Grid1.Item(a + 1, b + 1).Style.ForeColor = Color.Red
                    End If

                    Application.DoEvents()
                    Stringa_Salvataggio_Pattern_Preset = Stringa_Salvataggio_Pattern_Preset & " | "
                Next
            Next

            Grid1.Visible = True

            If Test = False Then
                Test_Cis = False
                Stringa_Errore = "Fallita verifica pattern CIS , premere un tasto per ripetere, Esc per uscire"
                Errore_Cis = "Fail " & " | " & stato_sens_bin & " | " & Front_CIS_Offset(1, 0) & " | " & Front_CIS_Offset(1, 1) & " | " & Front_CIS_Offset(1, 2) & " | " & Rear_CIS_Offset(1, 0) & " | " & Rear_CIS_Offset(1, 1) & " | " & Rear_CIS_Offset(1, 2) & " | "
                Errore_Cis = Errore_Cis & Front_CIS_Igen(1, 0) & " | " & Front_CIS_Igen(1, 1) & " | " & Front_CIS_Igen(1, 2) & " | " & Rear_CIS_Igen(1, 0) & " | " & Rear_CIS_Igen(1, 1) & " | " & Rear_CIS_Igen(1, 2) & " | "
                Errore_Cis = Errore_Cis & Front_CIS_Pwm(1, 0) & " | " & Front_CIS_Pwm(1, 1) & " | " & Front_CIS_Pwm(1, 2) & " | " & Rear_CIS_Pwm(1, 0) & " | " & Rear_CIS_Pwm(1, 1) & " | " & Rear_CIS_Pwm(1, 2) & "| Fail | "
                Exit Function
            End If
            Test = False
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass " & " | " & stato_sens_bin & " | " & Front_CIS_Offset(1, 0) & " | " & Front_CIS_Offset(1, 1) & " | " & Front_CIS_Offset(1, 2) & " | " & Rear_CIS_Offset(1, 0) & " | " & Rear_CIS_Offset(1, 1) & " | " & Rear_CIS_Offset(1, 2) & " | "
            Stringa_Salvataggio = Stringa_Salvataggio & Front_CIS_Igen(1, 0) & " | " & Front_CIS_Igen(1, 1) & " | " & Front_CIS_Igen(1, 2) & " | " & Rear_CIS_Igen(1, 0) & " | " & Rear_CIS_Igen(1, 1) & " | " & Rear_CIS_Igen(1, 2) & " | "
            Stringa_Salvataggio = Stringa_Salvataggio & Front_CIS_Pwm(1, 0) & " | " & Front_CIS_Pwm(1, 1) & " | " & Front_CIS_Pwm(1, 2) & " | " & Rear_CIS_Pwm(1, 0) & " | " & Rear_CIS_Pwm(1, 1) & " | " & Rear_CIS_Pwm(1, 2) & "| True | "
            Scrivi("Test pattern CIS superato!", Color.Green)
            Test_Cis = True
            Debug_test_control()

            Grid1.Visible = False

        Else
            'davide debug
            'Numero Canali = 4 
            '1. Esecuzione del comando Preset (83) che provoca la calibrazione dei sistemi ottici e degli altri sensori del lettore di banconote: teoricamente lo stato di uscita dovrebbe essere READY ma un eventuale valore differente potrebbe essere sintomo di qualche altra parte circuitale non funzionante.
            Traspsend("83", 1, 10000)
            'Upload delle due pagine di Spt che contengono i dati da analizzare con le seguenti istanze della richiesta Upload (D3 06):  
            'D3 06 06 00  64 x [0d 0d 0d 0d] – System Spt page #0  D3 06 06 01  64 x [0d 0d 0d 0d] – System Spt page #1

            Scrivi("Caricamento SPT 0....", Color.Black)
            Traspsend("D3060600", 256, 10000)
            Dim spt0 = TOGLIE_0(stringa_ricezione(Traspric))

            'Scrivi("Caricamento SPT 1....", Color.Black)
            'Traspsend("D3060601", 256, 10000)
            'Dim spt1 = TOGLIE_0(stringa_ricezione(Traspric))


            'Verificare che il contenuto della locazione 0x0004 della page #0 (da adesso si userà la convenzione [page:loc] ovvero [0:0x0004]) rispecchi una condizione di successo su tutti i test (ogni campo è rappresentato da 1 bit):
            Dim stato_sensori = Mid(spt0, 17, 4)

            cancella_griglia(Grid1)

            Grid1.RowCount = 2
            Grid1.ColumnCount = 16
            'Grid1.ColumnCount = 22

            Grid1.Item(0, 0).Value = "SF0"
            Grid1.Item(1, 0).Value = "SF1"
            Grid1.Item(2, 0).Value = "SF2"
            Grid1.Item(3, 0).Value = "SF3"
            Grid1.Item(4, 0).Value = "SF4"
            Grid1.Item(5, 0).Value = "SF5"
            Grid1.Item(6, 0).Value = "SF6"
            Grid1.Item(7, 0).Value = "Front_Dark"

            Grid1.Item(8, 0).Value = "SR0"
            Grid1.Item(9, 0).Value = "SR1"
            Grid1.Item(10, 0).Value = "SR2"
            Grid1.Item(11, 0).Value = "SR3"
            Grid1.Item(12, 0).Value = "SR4"
            Grid1.Item(13, 0).Value = "SR5"
            Grid1.Item(14, 0).Value = "SR6"
            Grid1.Item(15, 0).Value = "Rear_Dark"






            Dim stato_sens_bin = hex_bin(stato_sensori)

            For A = 0 To 15
                Grid1.Item(A, 1).Value = Mid(stato_sens_bin, 16 - A, 1)

                If Grid1(A, 1).Value = "1" Then
                    Grid1.Item(A, 1).Style.ForeColor = Color.Red
                Else
                    Grid1.Item(A, 1).Style.ForeColor = Color.Green
                End If
            Next

            Ridimensiona_Griglia(Grid1)

            Grid1.Visible = True

            If stato_sensori <> "0000" Then
                Test_Cis = False
                Errore_Cis = " Fail " & " | " & stato_sens_bin & " | "
                Stringa_Errore = "Fallito stato sensori, premere un tasto per ripere, Esc per uscire"
                Exit Function
            End If

            Scrivi("Verifica stato sensori Superata", Color.Green)

            Debug_test_control()
            Grid1.Visible = False

            'Con la richiesta GetSrcSymInfo (D3 1C 02) si verifica se esistono condizioni anomale di funzionamento dei generatori di corrente dei CIS.
            Traspsend("D31C02", 4, 10000)

            Dim Control_Front = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 3, 2)
            Dim Control_Rear = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 2)

            Grid1.RowCount = 2
            Grid1.ColumnCount = 2

            Grid1.Item(0, 0).Value = "Control Front"
            Grid1.Item(0, 1).Value = "Control Rear"
            '00:Not used
            '01:OK()
            '02:Fault()
            'Other():Not used
            Select Case Control_Front
                Case "00"
                    Grid1.Item(1, 0).Value = "Not used"
                    Grid1.Item(1, 0).Style.ForeColor = Color.Red
                Case "01"
                    Grid1.Item(1, 0).Value = "Ok"
                    Grid1.Item(1, 0).Style.ForeColor = Color.Green
                Case "02"
                    Grid1.Item(1, 0).Value = "Fault"
                    Grid1.Item(1, 0).Style.ForeColor = Color.Red
                Case Else
                    Grid1.Item(1, 0).Value = "Not used"
                    Grid1.Item(1, 0).Style.ForeColor = Color.Red
            End Select

            Select Case Control_Rear
                Case "00"
                    Grid1.Item(1, 1).Value = "Not used"
                    Grid1.Item(1, 1).Style.ForeColor = Color.Red
                Case "01"
                    Grid1.Item(1, 1).Value = "Ok"
                    Grid1.Item(1, 1).Style.ForeColor = Color.Green
                Case "02"
                    Grid1.Item(1, 1).Value = "Fault"
                    Grid1.Item(1, 1).Style.ForeColor = Color.Red
                Case Else
                    Grid1.Item(1, 1).Value = "Not used"
                    Grid1.Item(1, 1).Style.ForeColor = Color.Red
            End Select

            Ridimensiona_Griglia(Grid1)
            Grid1.Visible = True
            If Control_Front <> "01" Or Control_Rear <> "01" Then
                Test_Cis = False
                Errore_Cis = " Fail " & " | " & stato_sens_bin & " | " & Control_Rear & Control_Front & " | "
                Stringa_Errore = "Fallito controllo simmetria di illuminazione sensori, premere un tasto per ripere, Esc per uscire"
                Exit Function
            End If

            Scrivi("Verifica simmetria di illuminazione sensori Superata", Color.Green)

            Debug_test_control()
            Grid1.Visible = False


            Grid1.RowCount = 22
            Grid1.ColumnCount = 3

            cancella_griglia(Grid1)

            Grid1.Item(0, 0).Value = "SF0 Current"
            Grid1.Item(0, 1).Value = "SF1 Current"
            Grid1.Item(0, 2).Value = "SF2 Current"
            Grid1.Item(0, 3).Value = "SF3 Current"
            Grid1.Item(0, 4).Value = "SR0 Current"
            Grid1.Item(0, 5).Value = "SR1 Current"
            Grid1.Item(0, 6).Value = "SR2 Current"
            Grid1.Item(0, 7).Value = "SR3 Current"

            Grid1.Item(0, 8).Value = "SF0 PWM"
            Grid1.Item(0, 9).Value = "SF1 PWM"
            Grid1.Item(0, 10).Value = "SF2 PWM"
            Grid1.Item(0, 11).Value = "SF3 PWM"
            Grid1.Item(0, 12).Value = "SR0 PWM"
            Grid1.Item(0, 13).Value = "SR1 PWM"
            Grid1.Item(0, 14).Value = "SR2 PWM"
            Grid1.Item(0, 15).Value = "SR3 PWM"

            Grid1.Item(0, 16).Value = "Front CIS Offset (Seg 1)"
            Grid1.Item(0, 17).Value = "Front CIS Offset (Seg 2)"
            Grid1.Item(0, 18).Value = "Front CIS Offset (Seg 3)"
            Grid1.Item(0, 19).Value = "Rear CIS Offset (Seg 1)"
            Grid1.Item(0, 20).Value = "Rear CIS Offset (Seg 2)"
            Grid1.Item(0, 21).Value = "Rear CIS Offset (Seg 3)"


            Scrivi("Caricamento Correnti, PWM e Offset in corso...", Color.Black)

            Dim CurrentSCR(2) As String '0 attuale 1 nativo
            Mem_Dump(10, "00000400", "00000020")
            CurrentSCR(0) = TOGLIE_0(stringa_ricezione(Traspric))
            Dim salva_correnti(2) As String
            Dim salva_pwm(2) As String
            Dim salva_offset(2) As String
            Test = True

            Mem_Dump(10, "00000440", "00000020")
            CurrentSCR(1) = TOGLIE_0(stringa_ricezione(Traspric))

            Dim Valore, limite_basso, limite_alto As Long

            For a = 0 To 3

                Valore = Val("&h" & Mid$(CurrentSCR(0), (a * 4) + 1, 4))
                limite_basso = Val("&h" & Mid$(CurrentSCR(1), (a * 4) + 1, 4)) - IGEN_MAX_DELTA
                limite_alto = Val("&h" & Mid$(CurrentSCR(1), (a * 4) + 1, 4)) + IGEN_MAX_DELTA

                Grid1.Item(1, a).Value = Valore
                Grid1.Item(2, a).Value = "[" & limite_basso.ToString & "/" & limite_alto.ToString & "]"

                If Valore < limite_basso Or Valore > limite_alto Then
                    Grid1.Item(1, a).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a).Style.ForeColor = Color.Green
                End If
                salva_correnti(0) = salva_correnti(0) & Valore.ToString & " | "

                Valore = Val("&h" & Mid$(CurrentSCR(0), (a * 4) + 33, 4))
                limite_basso = Val("&h" & Mid$(CurrentSCR(1), (a * 4) + 33, 4)) - IGEN_MAX_DELTA
                limite_alto = Val("&h" & Mid$(CurrentSCR(1), (a * 4) + 33, 4)) + IGEN_MAX_DELTA

                Grid1.Item(1, a + 4).Value = Valore
                Grid1.Item(2, a + 4).Value = "[" & limite_basso.ToString & "/" & limite_alto.ToString & "]"

                If Valore < limite_basso Or Valore > limite_alto Then
                    Grid1.Item(1, a + 4).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 4).Style.ForeColor = Color.Green
                End If
                salva_correnti(1) = salva_correnti(1) & Valore.ToString & " | "
            Next



            Scrivi("Caricamento Correnti, PWM e Offset in corso...", Color.Black)

            Dim PwmSCR(2) As String '0 attuale 1 nativo
            Mem_Dump(10, "00000420", "00000020")
            PwmSCR(0) = TOGLIE_0(stringa_ricezione(Traspric))

            Mem_Dump(10, "00000460", "00000020")
            PwmSCR(1) = TOGLIE_0(stringa_ricezione(Traspric))

            For a = 0 To 3
                Valore = Val("&h" & Mid$(PwmSCR(0), (a * 4) + 1, 4))
                limite_basso = Val("&h" & Mid$(PwmSCR(1), (a * 4) + 1, 4)) - PWM_MAX_DELTA
                limite_alto = Val("&h" & Mid$(PwmSCR(1), (a * 4) + 1, 4)) + PWM_MAX_DELTA

                Grid1.Item(1, a + 8).Value = Valore
                Grid1.Item(2, a + 8).Value = "[" & limite_basso.ToString & "/" & limite_alto.ToString & "]"

                If Valore < limite_basso Or Valore > limite_alto Then
                    Grid1.Item(1, a + 8).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 8).Style.ForeColor = Color.Green
                End If

                salva_pwm(0) = salva_pwm(0) & Valore.ToString & " | "

                Valore = Val("&h" & Mid$(PwmSCR(0), (a * 4) + 33, 4))
                limite_basso = Val("&h" & Mid$(PwmSCR(1), (a * 4) + 33, 4)) - PWM_MAX_DELTA
                limite_alto = Val("&h" & Mid$(PwmSCR(1), (a * 4) + 33, 4)) + PWM_MAX_DELTA

                Grid1.Item(1, a + 12).Value = Valore
                Grid1.Item(2, a + 12).Value = "[" & limite_basso.ToString & "/" & limite_alto.ToString & "]"

                If Valore < limite_basso Or Valore > limite_alto Then
                    Grid1.Item(1, a + 12).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 12).Style.ForeColor = Color.Green
                End If

                salva_pwm(1) = salva_pwm(1) & Valore.ToString & " | "

            Next

            Dim OffsetSCR(2) As String '0 attuale 1 nativo
            Mem_Dump(10, "000004C2", "0000000C")
            OffsetSCR(0) = TOGLIE_0(stringa_ricezione(Traspric))

            Mem_Dump(10, "000004CE", "0000000C")
            OffsetSCR(1) = TOGLIE_0(stringa_ricezione(Traspric))

            For a = 0 To 2
                Valore = Val("&h" & Mid$(OffsetSCR(0), (a * 4) + 1, 4))
                limite_basso = Val("&h" & Mid$(OffsetSCR(1), (a * 4) + 1, 4)) - OFF_MAX_DELTA
                limite_alto = Val("&h" & Mid$(OffsetSCR(1), (a * 4) + 1, 4)) + OFF_MAX_DELTA

                Grid1.Item(1, a + 16).Value = Valore
                Grid1.Item(2, a + 16).Value = "[" & limite_basso.ToString & "/" & limite_alto.ToString & "]"

                If Valore < limite_basso Or Valore > limite_alto Then
                    Grid1.Item(1, a + 16).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 16).Style.ForeColor = Color.Green
                End If

                salva_offset(0) = salva_offset(0) & Valore.ToString & " | "

                Valore = Val("&h" & Mid$(OffsetSCR(0), (a * 4) + 13, 4))
                limite_basso = Val("&h" & Mid$(OffsetSCR(1), (a * 4) + 13, 4)) - OFF_MAX_DELTA
                limite_alto = Val("&h" & Mid$(OffsetSCR(1), (a * 4) + 13, 4)) + OFF_MAX_DELTA

                Grid1.Item(1, a + 19).Value = Valore
                Grid1.Item(2, a + 19).Value = "[" & limite_basso.ToString & "/" & limite_alto.ToString & "]"

                If Valore < limite_basso Or Valore > limite_alto Then
                    Grid1.Item(1, a + 19).Style.ForeColor = Color.Red
                    Test = False
                Else
                    Grid1.Item(1, a + 19).Style.ForeColor = Color.Green
                End If
                salva_offset(1) = salva_offset(1) & Valore.ToString & " | "

            Next

            Ridimensiona_Griglia(Grid1)
            Grid1.Visible = True
            Debug_test_control()

            If Test = False Then
                Test_Cis = False
                Errore_Cis = " Fail " & " | " & stato_sens_bin & " | " & Control_Rear & Control_Front & " | " & salva_correnti(0) & salva_correnti(1) & salva_pwm(0) & salva_pwm(1) & salva_offset(0) & salva_offset(1)
                Stringa_Errore = "Fallito controllo Correnti, Pwm e Offset, premere un tasto per ripere, Esc per uscire"
                Exit Function
            End If

            Dim vis_grid(4, 1) As Boolean
            For a = 0 To 4
                For b = 0 To 1
                    vis_grid(a, b) = True
                Next
            Next
            Scrivi("Controllo Correnti, Pwm e Offset superato", Color.Green)

            Grid1.Visible = False

            cancella_griglia(Grid1)
            Grid1.RowCount = 6
            Grid1.ColumnCount = 3
            'Grid1.ColumnCount = 5
            Grid1.Item(0, 1).Value = "Scanline Dark"
            Grid1.Item(0, 2).Value = "Scanline DSR0"
            Grid1.Item(0, 3).Value = "Scanline DSR1"
            Grid1.Item(0, 4).Value = "Scanline DSR2"
            Grid1.Item(0, 5).Value = "Scanline DSR3"
            Grid1.Item(1, 0).Value = " Front "
            Grid1.Item(2, 0).Value = " Rear "
            Ridimensiona_Griglia(Grid1)

            Scrivi("Caricamento dei pattern di preset, attendere", Color.Black)

            'Dim pattern_preset() As Byte = USBB("301F", 2, 65536, True)
            'Dim pattern_nativi_DARK() As Byte = USBB("69000000090032", 4, 65536, False)
            'Dim pattern_nativi() As Byte = USBB("69000000090033", 4, 65536, False)
            'Dim pattern_preset() As Byte = USBB("301F", 65536, 2, True)
            Dim pattern_preset() As Byte = USBB("301F", 65536, 5, True)
            Dim pattern_nativi_DARK() As Byte = USBB("69000000090032", 65536, 65536, False)
            Dim pattern_nativi() As Byte = USBB("69000000090033", 65536, 65536, False)
          
            'OPT_DARK_PATTERN_DELTA_PERC  OPT_WHITE_PATTERN_DELTA_PERC
            'DARK 
            Dim K As Int32 'sfasamento per preset
            Dim X As Int32 'sfasamento per nativi

            Try

           
                For A = 0 To 683
                    'Dark front
                    K = 0 : X = 0
                    If pattern_preset(A + K) < pattern_nativi_DARK(A + X) - (pattern_nativi_DARK(A + X) * OPT_DARK_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi_DARK(A + X) + (pattern_nativi_DARK(A + X) * OPT_DARK_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(0, 0) = False
                    End If
                    'Dark rear
                    K = 2048 * 2 : X = 2048 * 2
                    If pattern_preset(A + K) < pattern_nativi_DARK(A + X) - (pattern_nativi_DARK(A + X) * OPT_DARK_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi_DARK(A + X) + (pattern_nativi_DARK(A + X) * OPT_DARK_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(0, 1) = False
                    End If
                    'DSR0 front
                    K = 2048 * 4 : X = 0
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(1, 0) = False
                    End If
                    'DSR1 front
                    K = 2048 * 5 : X = 2048
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(2, 0) = False
                    End If
                    'DSR2 front
                    K = 2048 * 6 : X = 2048 * 2
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(3, 0) = False
                    End If
                    'DSR3 front
                    K = 2048 * 7 : X = 2048 * 3
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(4, 0) = False
                    End If

                    'DSR0 rear
                    K = 2048 * 12 : X = 2048 * 8
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(1, 1) = False
                    End If

                    'DSR1 rear
                    K = 2048 * 13 : X = 2048 * 9
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(2, 1) = False
                    End If

                    'DSR2 rear
                    K = 2048 * 14 : X = 2048 * 10
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(3, 1) = False
                    End If

                    'DSR3 rear
                    K = 2048 * 15 : X = 2048 * 11
                    If pattern_preset(A + K) < pattern_nativi(A + X) - (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Or pattern_preset(A + K) > pattern_nativi(A + X) + (pattern_nativi(A + X) * OPT_WHITE_PATTERN_DELTA_PERC / 100) Then
                        Test = False
                        vis_grid(4, 1) = False
                    End If
                Next
                Stringa_Salvataggio_Pattern_Preset = ""
                For a = 0 To 15
                    If a = 0 Or a = 2 Or a = 4 Or a = 5 Or a = 6 Or a = 7 Or a = 12 Or a = 13 Or a = 14 Or a = 15 Then
                        For b = 0 To 863

                            Stringa_Salvataggio_Pattern_Preset = Stringa_Salvataggio_Pattern_Preset & pattern_preset(a * 2048 + b) & " | "

                        Next
                        Stringa_Salvataggio_Pattern_Preset = Stringa_Salvataggio_Pattern_Preset & " | "
                    End If
                Next
            Catch ex As Exception
                MessageBox.Show(ex.ToString())
            End Try

            'MsgBox("debug1")
            For a = 0 To 4
                For b = 0 To 1
                    If vis_grid(a, b) = True Then
                        Grid1.Item(b + 1, a + 1).Value = "Pass"
                        Grid1.Item(b + 1, a + 1).Style.ForeColor = Color.Green
                    Else
                        Grid1.Item(b + 1, a + 1).Value = "Fail"
                        Grid1.Item(b + 1, a + 1).Style.ForeColor = Color.Red
                    End If
                Next
            Next
            'MsgBox("debug2")
            Grid1.Visible = True


            If Test = False Then

                Test_Cis = False
                Errore_Cis = " Fail " & " | " & stato_sens_bin & " | " & Control_Rear & Control_Front & " | " & salva_correnti(0) & salva_correnti(1) & salva_pwm(0) & salva_pwm(1) & salva_offset(0) & salva_offset(1) & "Fail |"
                Stringa_Errore = "Fallita verifica pattern CIS, premere un tasto per ripere, Esc per uscire"
                Exit Function
            End If
            Test = False
            Stringa_Salvataggio = Stringa_Salvataggio & "Pass " & " | " & stato_sens_bin & " | " & Control_Rear & Control_Front & " | " & salva_correnti(0) & salva_correnti(1) & salva_pwm(0) & salva_pwm(1) & salva_offset(0) & salva_offset(1) & "Pass |"
            Scrivi("Test pattern CIS superato!", Color.Green)
            Test_Cis = True
            Debug_test_control()

            Grid1.Visible = False
        End If


    End Function

    Public Function Test_Foto_Trasparenza() As Boolean

        Dim strDebug As String
        'strDebug = "davide"
        'ListBox1.Items.Add(strDebug)
        'MsgBox("OK")
        'ListBox1.Items.Add("loggia")
        'MsgBox("OK")

        Dim aa As String

        '[Fotosensore Ingresso attrezzo collaudo]
        Dim TRASP1_REF_DARK0 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_REF_DARK0")
        Dim TRASP2_REF_DARK0 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_REF_DARK0")

        Dim FOTIN_REF_DARK1_NEW As Integer = 250


        Dim TRASP1_CURRENT1 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_CURRENT1")
        Dim TRASP2_CURRENT1 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_CURRENT1")

        Dim TRASP1_REF_DARK1 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_REF_DARK1")
        Dim TRASP2_REF_DARK1 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_REF_DARK1")

        Dim TRASP1_REF_LIGHT1 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_REF_LIGHT1")
        Dim TRASP2_REF_LIGHT1 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_REF_LIGHT1")

        Dim TRASP1_CURRENT2 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_CURRENT2")
        Dim TRASP2_CURRENT2 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_CURRENT2")

        Dim TRASP1_REF_DARK2 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_REF_DARK2")
        Dim TRASP2_REF_DARK2 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_REF_DARK2")

        Dim TRASP1_REF_LIGHT2 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP1_REF_LIGHT2")
        Dim TRASP2_REF_LIGHT2 As Integer = Read_ParPiastra("Fotosensore trasparenza attrezzo collaudo", "TRASP2_REF_LIGHT2")
        Grid1.ColumnCount = 4
        Grid1.Rows.Clear()
        Grid1.Rows.Add("#Verifica", "REF", "CURRENT", "FOTTRASP")
        Grid1.Visible = True

        If (Attesa_Tasto = True) Then
            Label6.Visible = True
            Label7.Visible = True
            Label8.Visible = True
            Label8.Visible = True
            Label9.Visible = True
            Label10.Visible = True
            Label11.Visible = True
            Label12.Visible = True
            Label13.Visible = True
            Label14.Visible = True
            Label15.Visible = True
            Label16.Visible = True
            Label17.Visible = True

            Label6.Text = ""
            Label7.Text = ""
            Label8.Text = ""
            Label9.Text = ""
            Label10.Text = ""
            Label11.Text = ""
            Label12.Text = ""
            Label13.Text = ""
            Label14.Text = ""
            Label15.Text = ""
            Label16.Text = ""
            Label17.Text = ""
        End If






        '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 
        '2.impostare con il comando MemWrite (D3 33 1…) l’abilitazione del primo ramo del generatore di corrente per la trasparenza;
        '3.Impostare con il comando MemWrite la corrente del foto emettitore a 0 (zero) ed attendere un minimo di 5 msec;
        Test = False
        Scrivi("Reboot in corso....", Color.Black)
        Reboot()
        Test_Foto_Trasparenza = False
        Scrivi("Test Fotosensore di trasparenza in corso....", Color.Black)
        'Call(Traspsend("D3070400", 1, 200)) '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 
        Call Traspsend("D3070400", 1, 2000) '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 

        Mem_Write(1, "84000000", "00000000", "00000000")
        MySleep(5)
        'MySleep(150)
        Mem_Write(1, "84000000", "00000004", "00020000")
        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000004", "00000004")

        aa = TOGLIE_0(stringa_ricezione(Traspric))
        Dim kk As Long = CLng("&hFFFCFC00")
        aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0

        aa = dec_hex("&h" & aa Or &H10000).ToString 'alzo il bit led

        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If


        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 0 e metto corrente a 0

        MySleep(5)
        'MySleep(150)

        '5Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_DARK0 ed attendere un minimo di 5 msec;
        '6.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1 (NOTE_IN);
        Mem_Dump(1, "84000000", "00000004")
        aa = aa
        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP1_REF_DARK0) & "00"

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        If (Attesa_Tasto = True) Then
            strDebug = "5->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   6->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label6.Text = strDebug
        End If


        Mem_Dump(1, "84000000", "00000004")



        If (Attesa_Tasto = True) Then
            Attendi_Tasto()
        End If


        Grid1.Rows.Add("F1-1", "DARK0=" & TRASP1_REF_DARK0, "CUR1=" & TRASP1_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
            Stringa_Errore = "Test Fotosensore trasparenza1 fallito, fallita verifica 1, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Fail_Step1_1 | "
            Exit Function

        End If



        'davide inizio
        'aggiungo un ulteriore ref a 250 per problematiche..ma non guardo il valore di uscita
        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_DARK1_NEW) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 6) & dec_hex(TRASP1_CURRENT1)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")
        'davide fine



        '7.mpostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_DARK1 ed attendere un minimo di 5 msec;
        '8.Impostare con il comando MemWrite la corrente del foto emettitore a TRASP1_CURRENT1 ed attendere un minimo di 5 msec;
        '9.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP1_REF_DARK1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000004", "00000004")

        aa = TOGLIE_0(stringa_ricezione(Traspric))

        kk = CLng("&hFFFCFC00")
        aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

        aa = dec_hex(("&h" & aa) Or &H10000 Or TRASP1_CURRENT1) 'alzo il bit di led0 e imposto la corrente1

        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If

        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 0 e metto corrente1

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")



        If (Attesa_Tasto = True) Then
            strDebug = "7->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   8->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label7.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F1-2", "DARK1=" & TRASP1_REF_DARK1, "CUR1=" & TRASP1_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
            'MsgBox(Traspric.ToString())
            Stringa_Errore = "Test Fotosensore trasparenza1 fallito, fallita verifica 2, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Fail_Step2_1 | "
            'Exit Function
            MySleep(150)
            ' MsgBox("Premere Ok per continuare - step 0 ")
            Call Traspsend("D3070400", 1, 2000) '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 

            Mem_Dump(1, "84000000", "00000004")

            aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP1_REF_DARK1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

            Mem_Write(1, "84000000", "00000004", aa)

            MySleep(5)
            'MySleep(150)

            Mem_Dump(1, "84000004", "00000004")

            aa = TOGLIE_0(stringa_ricezione(Traspric))

            kk = CLng("&hFFFCFC00")
            aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

            aa = dec_hex(("&h" & aa) Or &H10000 Or TRASP1_CURRENT1) 'alzo il bit di led0 e imposto la corrente1

            If aa.Length <> 8 Then
                For a = aa.Length To 7
                    aa = "0" & aa
                Next
            End If

            Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 0 e metto corrente1

            MySleep(5)
            'MySleep(150)

            Mem_Dump(1, "84000000", "00000004")

            If (Attesa_Tasto = True) Then
                strDebug = "7A->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   8A->   " + Mid(stringa_ricezione(Traspric), 8, 1)
                Label17.Text = strDebug

                Attendi_Tasto()
            End If

            Grid1.Rows.Add("F1-2", "DARK1=" & TRASP1_REF_DARK1, "CUR1=" & TRASP1_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
            Ridimensiona_Griglia(Grid1)
            If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
                Stringa_Errore = "Test Fotosensore trasparenza1 fallito, fallita verifica 2, premere un tasto per ripetere, Esc per uscire"
                Call Traspsend("D3070401", 1, 200)
                Call Traspsend("D30703", 0, 0)
                attendi_per("41")
                Errore_Foto_Trasparenza = "Fail_Step2_1 | "
                Exit Function

            End If

        End If

        '10.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_LIGHT1 ed attendere un minimo di 5 msec;
        '11.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 0;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP1_REF_LIGHT1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        If (Attesa_Tasto = True) Then
            strDebug = "10->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   11->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label8.Text = strDebug
        End If

        Grid1.Rows.Add("F1-3", "LIGHT1=" & TRASP1_REF_LIGHT1, "CUR1=" & TRASP1_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "0" Then
            Stringa_Errore = "Test Fotosensore trasparenza1 fallito, fallita verifica 3, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Fail_Step3_1 | "
            Exit Function

        End If

        '12.Impostare con il comando MemWrite la corrente del foto emettitore a TRASP1_CURRENT2 ed attendere un minimo di 5 msec;
        '13.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_DARK2 ed attendere un minimo di 5 msec;
        '14.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;
        Mem_Dump(1, "84000004", "00000004")

        aa = TOGLIE_0(stringa_ricezione(Traspric))

        kk = CLng("&hFFFCFC00")
        aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

        aa = dec_hex(("&h" & aa) Or &H10000 Or TRASP1_CURRENT2) 'alzo il bit di led0 e imposto la corrente1

        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If

        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 0 e metto corrente1

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP1_REF_DARK2) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        If (Attesa_Tasto = True) Then
            strDebug = "12->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   13->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label9.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F1-4", "DARK2=" & TRASP1_REF_DARK2, "CUR2=" & TRASP1_CURRENT2, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
            Stringa_Errore = "Test Fotosensore trasparenza1 fallito, fallita verifica 4, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Fail_Step4_1 | "
            Exit Function

        End If

        '15.mpostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_LIGHT2 ed attendere un minimo di 5 msec;
        '16.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 0;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP1_REF_LIGHT2) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        If (Attesa_Tasto = True) Then
            strDebug = "15->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   16->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label10.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F1-5", "LIGHT2=" & TRASP1_REF_LIGHT2, "CUR2=" & TRASP1_CURRENT2, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "0" Then
            Stringa_Errore = "Test Fotosensore trasparenza1 fallito, fallita verifica 5, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Fail_Step5_1 | "
            Exit Function

        End If


        'trasparenza2!!!!!!!!!!!!!!!!!!!

        '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 
        '2.impostare con il comando MemWrite (D3 33 1…) l’abilitazione del primo ramo del generatore di corrente per la trasparenza;
        '1.Impostare con il comando MemWrite la corrente del foto emettitore a 0 (zero) ed attendere un minimo di 5 msec;

        'Scrivi("Reboot in corso....", Color.Black)me lo ha fatto togliere dario contro il mio parere (vale anche per gli altri 2)
        'Reboot()
        Test_Foto_Trasparenza = False
        Scrivi("Test Fotosensore di trasparenza2 in corso....", Color.Black)

        'davide Call Traspsend("D3070400", 1, 2000) '1.Disabilitazione del sincronismo di acquisizione con il comando EnableNoteIn(OFF) (D3 07 04 00). 
        'MsgBox("Premere Ok per Contonuare")
        MySleep(150)
        Call Traspsend("D3070400", 1, 2000)

        Mem_Write(1, "84000000", "00000000", "00000000")
        MySleep(5)
        'MySleep(150)
        'me lo ha fatto togliere dario contro il mio parere
        Mem_Write(1, "84000000", "00000000", "00020000")
        MySleep(5)
        'MySleep(150)

        Mem_Write(1, "84000000", "00000004", "00010000")
        MySleep(5)
        'MySleep(150)
        Mem_Dump(1, "84000004", "00000004")
        aa = TOGLIE_0(stringa_ricezione(Traspric))

        kk = CLng("&hFFFCFC00")
        aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

        aa = dec_hex(("&h" & aa) Or &H20000)  'alzo il bit di led0 e imposto la corrente1

        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If

        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 1 e metto corrente a 0

        MySleep(5)
        'MySleep(150)

        '5Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP2_REF_DARK0 ed attendere un minimo di 5 msec;
        '6.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1 (NOTE_IN);
        Mem_Dump(1, "84000000", "00000004")
        aa = aa
        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP2_REF_DARK0) & "00"

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        If (Attesa_Tasto = True) Then
            strDebug = "5->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "6->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label11.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F2-1", "DARK0=" & TRASP2_REF_DARK0, "CUR1=" & TRASP2_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
            Stringa_Errore = "Test Fotosensore trasparenza2 fallito, fallita verifica 1, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Pass | Fail_Step1_2 | "
            Exit Function

        End If

        'davide inizio
        'aggiungo un ulteriore ref a 250 per problematiche..ma non guardo il valore di uscita
        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(FOTIN_REF_DARK1_NEW) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 6) & dec_hex(TRASP2_CURRENT1)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")
        'davide fine


        '7.mpostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP2_REF_DARK1 ed attendere un minimo di 5 msec;
        '8.Impostare con il comando MemWrite la corrente del foto emettitore a TRASP2_CURRENT1 ed attendere un minimo di 5 msec;
        '9.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;


        'AGGIUNTO DELAY !
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP2_REF_DARK1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000004", "00000004")
        aa = TOGLIE_0(stringa_ricezione(Traspric))

        kk = CLng("&hFFFCFC00")
        aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

        aa = dec_hex(("&h" & aa) Or &H20000 Or TRASP2_CURRENT1) 'alzo il bit di led0 e imposto la corrente1

        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If

        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 1 e metto corrente1

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")


        If (Attesa_Tasto = True) Then
            strDebug = "7->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   8->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label12.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F2-2", "DARK1=" & TRASP2_REF_DARK1, "CUR1=" & TRASP2_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
            Stringa_Errore = "Test Fotosensore trasparenza2 fallito, fallita verifica 2, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Pass | Fail_Step2_2 | "

            MySleep(10)
            'riprovo a leggere
            '7.mpostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP2_REF_DARK1 ed attendere un minimo di 5 msec;
            '8.Impostare con il comando MemWrite la corrente del foto emettitore a TRASP2_CURRENT1 ed attendere un minimo di 5 msec;
            '9.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;

            'Exit Function
            'DDD
            Call Traspsend("D3070400", 1, 2000)

            Mem_Dump(1, "84000000", "00000004")

            aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP2_REF_DARK1) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

            Mem_Write(1, "84000000", "00000004", aa)

            MySleep(5)
            'MySleep(150)

            Mem_Dump(1, "84000004", "00000004")
            aa = TOGLIE_0(stringa_ricezione(Traspric))

            kk = CLng("&hFFFCFC00")
            aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

            aa = dec_hex(("&h" & aa) Or &H20000 Or TRASP2_CURRENT1) 'alzo il bit di led0 e imposto la corrente1

            If aa.Length <> 8 Then
                For a = aa.Length To 7
                    aa = "0" & aa
                Next
            End If

            Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 1 e metto corrente1

            MySleep(5)
            'MySleep(150)

            Mem_Dump(1, "84000000", "00000004")

            If (Attesa_Tasto = True) Then
                strDebug = "7A->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   8A->   " + Mid(stringa_ricezione(Traspric), 8, 1)
                Label16.Text = strDebug

                Attendi_Tasto()
            End If

            Grid1.Rows.Add("F2-2", "DARK1=" & TRASP2_REF_DARK1, "CUR1=" & TRASP2_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
            Ridimensiona_Griglia(Grid1)
            If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
                Stringa_Errore = "Test Fotosensore trasparenza2 fallito, fallita verifica 2, premere un tasto per ripetere, Esc per uscire"
                Call Traspsend("D3070401", 1, 200)
                Call Traspsend("D30703", 0, 0)
                attendi_per("41")
                Errore_Foto_Trasparenza = "Pass | Fail_Step2_2 | "
                Exit Function

            End If


        End If




        '10.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_LIGHT1 ed attendere un minimo di 5 msec;
        '11.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 0;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP2_REF_LIGHT1) & "00"

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        If (Attesa_Tasto = True) Then
            strDebug = "10->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   11->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label13.Text = strDebug
        End If

        Grid1.Rows.Add("F2-3", "LIGHT1=" & TRASP2_REF_LIGHT1, "CUR1=" & TRASP2_CURRENT1, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "0" Then
            Stringa_Errore = "Test Fotosensore trasparenza2 fallito, fallita verifica 3, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Pass | Fail_Step3_2 | "
            Exit Function

        End If

        '12.Impostare con il comando MemWrite la corrente del foto emettitore a TRASP1_CURRENT2 ed attendere un minimo di 5 msec;
        '13.Impostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_DARK2 ed attendere un minimo di 5 msec;
        '14.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 1;

        'AGGIUNTO DELAY !
        'MySleep(150)

        Mem_Dump(1, "84000004", "00000004")

        aa = TOGLIE_0(stringa_ricezione(Traspric))

        kk = CLng("&hFFFCFC00")
        aa = dec_hex(("&h" & aa) And kk) 'azzero i bit del foto e la corrente a 0"

        aa = dec_hex(("&h" & aa) Or &H20000 Or TRASP2_CURRENT2) 'alzo il bit di led0 e imposto la corrente1

        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If

        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 0 e metto corrente1

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP2_REF_DARK2) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        'NEW
        'MySleep(150)

        If (Attesa_Tasto = True) Then
            strDebug = "12->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   13->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label14.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F2-4", "DARK2=" & TRASP2_REF_DARK2, "CUR2=" & TRASP2_CURRENT2, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "1" Then
            Stringa_Errore = "Test Fotosensore trasparenza2 fallito, fallita verifica 4, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Pass | Fail_Step4_2 | "
            Exit Function

        End If

        '15.mpostare con il comando MemWrite (D3 33 1…) il riferimento del comparatore al valore TRASP1_REF_LIGHT2 ed attendere un minimo di 5 msec;
        '16.Verificare con la richiesta MemDump (D3 33 0…) che il flag di uscita del comparatore sia 0;

        Mem_Dump(1, "84000000", "00000004")

        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) & dec_hex(TRASP2_REF_LIGHT2) & Mid(TOGLIE_0(stringa_ricezione(Traspric)), 7, 2)

        Mem_Write(1, "84000000", "00000004", aa)

        MySleep(5)
        'MySleep(150)

        Mem_Dump(1, "84000000", "00000004")

        If (Attesa_Tasto = True) Then
            strDebug = "15->   " + Mid(TOGLIE_0(stringa_ricezione(Traspric)), 1, 4) + "   16->   " + Mid(stringa_ricezione(Traspric), 8, 1)
            Label15.Text = strDebug

            Attendi_Tasto()
        End If

        Grid1.Rows.Add("F2-5", "LIGHT2=" & TRASP2_REF_LIGHT2, "CUR2=" & TRASP2_CURRENT2, Mid(stringa_ricezione(Traspric), 8, 1))
        Ridimensiona_Griglia(Grid1)
        If Mid(stringa_ricezione(Traspric), 8, 1) <> "0" Then
            Stringa_Errore = "Test Fotosensore trasparenza2 fallito, fallita verifica 5, premere un tasto per ripetere, Esc per uscire"
            Call Traspsend("D3070401", 1, 200)
            Call Traspsend("D30703", 0, 0)
            attendi_per("41")
            Errore_Foto_Trasparenza = "Pass | Fail_Step5_2 | "
            Exit Function

        End If

        Stringa_Salvataggio = Stringa_Salvataggio & "Pass | Pass | "
        kk = CLng("&hFFFCFC00")
        'aa = dec_hex("&h" & aa) And kk 'azzero i bit del foto e la corrente a 
        aa = dec_hex(("&h" & aa) And kk)
        If aa.Length <> 8 Then
            For a = aa.Length To 7
                aa = "0" & aa
            Next
        End If

        Mem_Write(1, "84000004", "00000004", aa) 'attivo il foto 0 e metto corrente1

        Call Traspsend("D3070401", 1, 200)
        Call Traspsend("D30703", 0, 0)
        attendi_per("41")
        Scrivi("Test Fotosensore trasparenza superato", Color.Green)
        Test = True
        Test_Foto_Trasparenza = True
        Debug_test_control()
        '15.Riabilitazione del sincronismo di acquisizione EnableNoteIn(ON).

        'spostati dopo il debug_test_control per vedere i dati in fase di debug
        'Grid1.Rows.Clear()
        'Grid1.Visible = False 
    End Function

    Public Function Verifica_FW()

dwl_all:
        Dim RS2x_FW_NAME_HOST As String = Read_ParPiastra("Firmware Finali", "RS2x_FW_NAME_HOST")
        Dim RS2x_FW_NAME_DSP As String = Read_ParPiastra("Firmware Finali", "RS2x_FW_NAME_DSP")
        Dim RS2x_FW_NAME_FPGA As String = Read_ParPiastra("Firmware Finali", "RS2x_FW_NAME_FPGA")

        Traspsend("E322", 13, 2000)
        Dim fpga As String = Mid(Traspric, 1, 8)

        No_Timeout = True
        Call Traspsend("E32B", 13, 1000)
        No_Timeout = False

        If Traspric.Length <> 13 Then
            Traspsend("E320", 13, 2000)
        End If


        Dim host As String = Mid(Traspric, 1, 8)
        Traspsend("E321", 13, 2000)
        Dim dsp As String = Mid(Traspric, 1, 8)
        Verifica_FW = False

        If fpga <> RS2x_FW_NAME_FPGA Then
            If DownLoad(WorkDir & "Firmware\" & RS2x_FW_NAME_FPGA, Label1, ProgressBar1) = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Call MySleep(1000)
                Call Cambio_Velocita_Seriale(1, True)
                Call MySleep(500)
                attendi_per_t("41", 1000, 10000)
                If Tasto <> 27 Then GoTo dwl_all
                GoTo fine
            End If
        End If

        Scrivi("Download Host in corso...", Color.Black)

        If host <> RS2x_FW_NAME_HOST Then
            If DownLoad(WorkDir & "Firmware\" & RS2x_FW_NAME_HOST, Label1, ProgressBar1) = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Call MySleep(1000)
                Call Cambio_Velocita_Seriale(1, True)
                Call MySleep(500)
                attendi_per_t("41", 1000, 10000)
                If Tasto <> 27 Then GoTo dwl_all
                GoTo fine
            End If
        End If


        If dsp <> RS2x_FW_NAME_DSP Then
            If DownLoad(WorkDir & "Firmware\" & RS2x_FW_NAME_DSP, Label1, ProgressBar1) = False Then
                Scrivi(Stringa_Errore, Color.Red)
                Attendi_Tasto()
                Call MySleep(1000)
                Call Cambio_Velocita_Seriale(1, True)
                Call MySleep(500)
                attendi_per_t("41", 1000, 1000)
                If Tasto <> 27 Then GoTo dwl_all
                GoTo fine
            End If
        End If
        Scrivi("Verifica FW superata", Color.Green)
        Verifica_FW = True
fine:
        If Verifica_FW = True Then
            Stringa_Salvataggio = Stringa_Salvataggio & fpga & " | " & host & " | " & dsp & " | " & "Pass | "
        Else
            Errore_FW = fpga & " | " & host & " | " & dsp & " | " & "Fail | "
        End If
    End Function

    Private Sub Text2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Tasto = e.KeyCode
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Text1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyDown
        Tasto = e.KeyCode
    End Sub

    Public Sub salva_dati(ByVal test As Boolean)

        Dim Test_Piastra As String
        Dim Data As String = DateTime.Now.Date
        Dim Ora As String = TimeOfDay
        Dim Stringa_Dati As String
        Dim Intestazione As String
        Dim Numero_canali As Integer = Read_ParPiastra("Ottica attrezzo di collaudo", "numero_canali")
        If test = True Then Test_Piastra = " Pass | " Else Test_Piastra = " Fail | "
        If Numero_canali = 3 Then
            Dim matr As String = Piastra.Matricola_Piastra & " | " & Piastra.Nome_operatore & " | " & Data & " | " & Ora & " | " & Test_Piastra
            Intestazione = "Matricola_Piastra | Nome_Operatore | Data | Ora | Test_Piastra | Connessione_Result | Test Micro SD | Trasferimento_FW_Result |Test_Comunicazione_Result | DWL_RIferimento_Result | Foto_In_Result | Encoder_Result |Encoder_Min | Encoder_Med | Encoder_Max | Test_UV_Result | Offset_LeftRight |Offset_Right | Igen_Left | Igen_Right | Min_Left | Med_Left | Max_Left | Min_Right | Med_Right | Max_Right | "
            Intestazione = Intestazione & "Cis_Result | Stato_Sensori | Front_CIS_Offset_1 |  Front_CIS_Offset_2 | Front_CIS_Offset_3 |  Rear_CIS_Offset_1 | Rear_CIS_Offset_2 | Rear_CIS_Offset_3 | "
            Intestazione = Intestazione & "Front_CIS_Igen_Ir | Front_CIS_Igen_Gr | Front_CIS_Igen_Rd | Rear_CIS_Igen_Ir | Rear_CIS_Igen_Gr | Rear_CIS_Igen_Rd | "
            Intestazione = Intestazione & "Front_CIS_Pwm_Ir | Front_CIS_Pwm_Gr | Front_CIS_Pwm_Rd | Rear_CIS_Pwm_Ir | Rear_CIS_Pwm_Gr | Rear_CIS_Pwm_Rd | Verifica_Pattern | "
            Intestazione = Intestazione & "Test_Tape | Test_Mag | Test SPI | Foto_Trasp_1 | Foto_Trasp_2 | Scrittura_Verifica_Matr | FPGA | HOST | DSP | Verifica FW | | Dark_Front | GR_Front | IR_Front | RD_Front  | Dark_Rear | GR_Rear | IR_Rear | RD_Rear"
            Dim M As Integer = 0
            For a = 1 To Intestazione.Length - 1
                If Mid(Intestazione, a, 1) = "|" Then M = M + 1
            Next

            M = M - 8

            Dim N As Integer = 0
            Stringa_Dati = matr & Stringa_Salvataggio

            For a = 1 To stringa_dati.Length - 1
                If Mid(stringa_dati, a, 1) = "|" Then N = N + 1
            Next

            For a = 1 To M - N
                Stringa_Dati = Stringa_Dati & "|"
            Next

        Else
            'Errore_Cis = " False " & " | " & stato_sens_bin & " | " & Control_Rear & Control_Front & " | " & salva_correnti(0) & salva_correnti(1) & salva_pwm(0) & salva_pwm(1) & salva_offset(0) & salva_offset(1)
            Dim matr As String = Piastra.Matricola_Piastra & " | " & Piastra.Nome_operatore & " | " & Data & " | " & Ora & " | " & Test_Piastra
            Intestazione = "Matricola_Piastra | Nome_Operatore | Data | Ora | Test_Piastra | Connessione_Result | Test Micro SD | Trasferimento_FW_Result |Test_Comunicazione_Result | DWL_RIferimento_Result | Foto_In_Result | Encoder_Result |Encoder_Min | Encoder_Med | Encoder_Max | Test_UV_Result | Offset_LeftRight |Offset_Right | Igen_Left | Igen_Right | Min_Left | Med_Left | Max_Left | Min_Right | Med_Right | Max_Right | "
            Intestazione = Intestazione & "Cis_Result | Stato_Sensori | Simmetria_sensori |  CIS_Igen_SF0 | CIS_Igen_SF1 |  CIS_Igen_SF2 | CIS_Igen_SF3 | CIS_Igen_SR0 | "
            Intestazione = Intestazione & "CIS_Igen_SR1 | CIS_Igen_SR2 | CIS_Igen_SR3 | CIS_Pwm_SF0 | CIS_Pwm_SF1  | CIS_Pwm_SF2  | CIS_Pwm_SF3 | CIS_Pwm_SR0 | CIS_Pwm_SR1 | CIS_Pwm_SR2 | CIS_Pwm_SR3 | "
            Intestazione = Intestazione & "Front_CIS_Offset_1 | Front_CIS_Offset_2 | Front_CIS_Offset_3 | Rear_CIS_Offset_1 | Rear_CIS_Offset_2 | Rear_CIS_Offset_3 | Verifica_Pattern | "
            Intestazione = Intestazione & "Test_Tape | Test_Mag | Test SPI | Foto_Trasp_1 | Foto_Trasp_2 | Scrittura_Verifica_Matr | FPGA | HOST | DSP | Verifica FW | | Dark_Front | Dark_Rear | SF0 | SF1  | SF2 | SF3 | SR0 | SR1 | SR2 | SR3"
            Dim M As Integer = 0
            For a = 1 To Intestazione.Length - 1
                If Mid(Intestazione, a, 1) = "|" Then M = M + 1
            Next

            M = M - 9

            Dim N As Integer = 0
            Stringa_Dati = matr & Stringa_Salvataggio

            For a = 1 To stringa_dati.Length - 1
                If Mid(stringa_dati, a, 1) = "|" Then N = N + 1
            Next

            For a = 1 To M - N
                Stringa_Dati = Stringa_Dati & "|"
            Next
        End If

        stringa_dati = stringa_dati & Stringa_Salvataggio_Pattern_Preset

        Dim myfile = Dir(WorkDir & "Log_" & Cod_piastra & "_" & Matr_attrezzatura & ".txt")

        Dim objWriter As New System.IO.StreamWriter(WorkDir & "Log_" & Cod_piastra & "_" & Matr_attrezzatura & ".txt", True)

        If myfile = "" Then objWriter.WriteLine(Intestazione.Replace(" ", ""))

        objWriter.WriteLine(stringa_dati.Replace(" ", ""))
        objWriter.Close()


    End Sub


    Private Sub Principale_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        Button1.Top = Me.Height / 2
        Button1.Left = (Me.Width - Button1.Width) / 2
        ProgressBar1.Top = Button1.Top
        Label2.Top = Button1.Top
    End Sub

    Private Sub VersioneToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VersioneToolStripMenuItem.Click
        AboutBox1.Visible = True
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        No_Timeout = True
        Connessione_Automatica()
        No_Timeout = False
        For a = 1 To 100
            Cambio_Velocita_Seriale(2, True)
            Reboot()
            Cambio_Velocita_Seriale(1, True)
        Next

    End Sub



    Private Sub SerialPort1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub open_port()
        moRS232 = New Rs232()
        Try
            '// Setup parameters
            With moRS232
                .Port = CInt(Open_Com)
                .BaudRate = Int32.Parse(baudrate)
                .DataBit = 8
                .StopBit = Rs232.DataStopBit.StopBit_1
                .Parity = Rs232.DataParity.Parity_None
                .Timeout = Int32.Parse(1000)
            End With
            '// Initializes port
            moRS232.Open()

            'If chkEvents.Checked Then moRS232.EnableEvents()
            'chkEvents.Enabled = True
        Catch Ex As Exception
            MessageBox.Show(Ex.Message, "Connection Error", MessageBoxButtons.OK)
        Finally
           
            'btnCheck.Enabled = moRS232.IsOpen
        End Try
    End Sub

    Private Sub close_port()

        moRS232.Close()
        
    End Sub

    Public Sub Traspsend(ByVal Out As String, ByVal NByte As Integer, ByVal Tout As Integer)
        Dim sTx As String
        '----------------------
        '// Clear Tx/Rx Buffers
        moRS232.PurgeBuffer(Rs232.PurgeBuffers.TxClear Or Rs232.PurgeBuffers.RXClear)
        moRS232.RxBufferThreshold = Int32.Parse(NByte)
        moRS232.Timeout = Val(Tout)
        sTx = stringa_invio(Out)
        moRS232.Write(sTx)
        'moRS232.Write(Chr(2) & Chr(2) & Chr(73) & Chr(48) & Chr(121) & Chr(3))
        '// Clears Rx textbox
        If NByte > 0 Then
            Ricezione(NByte)
        Else
            MySleep(Tout)
        End If
    End Sub

    Private Sub Ricezione(ByVal nbyte As Integer)

        moRS232.Read(Int32.Parse(nbyte))
        Traspric = moRS232.InputStreamString.ToString
        If Stringa_vuota = True Then Traspric = ""
        '
    End Sub
    Private Sub moRS232_CommEvent(ByVal source As Rs232, ByVal Mask As Rs232.EventMasks) Handles moRS232.CommEvent
        '===================================================
        '												©2003 www.codeworks.it All rights reserved
        '
        '	Description	:	Events raised when a comunication event occurs
        '	Created			:	15/07/03 - 15:13:46
        '	Author			:	Corrado Cavalli
        '
        '						*Parameters Info*
        '
        '	Notes				:	
        '===================================================
        Debug.Assert(Me.InvokeRequired = False)

        Dim iPnt As Int32, Buffer() As Byte
        Debug.Assert(Me.InvokeRequired = False)

        If (Mask And Rs232.EventMasks.RxChar) > 0 Then

            Buffer = source.InputStream
            For iPnt = 0 To Buffer.Length - 1

            Next

        End If

    End Sub

#Region "UI update routine"
#End Region



    Public Sub Mem_Dump(ByVal DevId As Byte, ByVal BlockAdr As String, ByVal BlockSize As String)

        BlockAdr = METTE_0(BlockAdr)
        Dim BlockSizes As String = METTE_0(BlockSize)
        Dim aa As String
        aa = "D3330" & Hex(DevId) & BlockAdr & BlockSizes
        Traspsend(aa, (CLng("&h" & BlockSize) * 2), 15000)
    End Sub

    Public Sub Mem_Write(ByVal DevId As Byte, ByVal BlockAdr As String, ByVal BlockSize As String, ByVal DataStream As String)

        BlockAdr = METTE_0(BlockAdr)
        BlockSize = METTE_0(BlockSize)
        DataStream = METTE_0(DataStream)
        Dim aa As String
        aa = "D3331" & DevId.ToString & BlockAdr & BlockSize & DataStream

        traspsend(aa, 0, 100)
    End Sub

    Public Function Connessione_Automatica() As Byte
        Dim aakk As Integer

        For b = 0 To 4
            moRS232.Close()
            Select Case b
                Case 0
                    moRS232.BaudRate = 9600

                Case 1
                    moRS232.BaudRate = 19200

                Case 2
                    moRS232.BaudRate = 38400

                Case 3
                    moRS232.BaudRate = 57600

                Case 4
                    moRS232.BaudRate = 115200

            End Select
            moRS232.Open()

ss:
            'moRS232.PurgeBuffer(PURGE_RXCLEAR)
            Status.Host = ""
            Traspric = ""
            Call Traspsend("F3", 0, 400)

            If moRS232.InBufferCount = 1 Then
                moRS232.Read(1)
                Status.Host = stringa_ricezione(moRS232.InputStreamString)
            End If

            aakk = moRS232.BaudRate

            If Val("&h" & Status.Host) > 127 Then
                Exit For
            End If
            If Status.Host <> "" Then
                Dim aa As String
                Select Case Status.Host

                    Case "00"

                    Case "41"
                        aa = attendi_per_t("41", 100, 6000)
                        If aa = False Then
                            Stringa_di_connessione = "Lettore in stato Busy, impossibile continuare, premere un tasto"
                            Connessione_Automatica = 0
                            Exit Function
                        End If
                        If aa = True Then GoTo ss
                    Case Else
                        aa = moRS232.BaudRate
                        For c = 1 To 8
                            Call Traspsend("F3", 1, 100)
                            If Status.Host <> stringa_ricezione(Traspric) Then
                                Exit For
                            End If
                        Next

                        Call Auto_Answ_ON()


                        Call Cambio_Velocita_Seriale(1, True)
                        Connessione_Automatica = 16
                        Exit Function

                End Select
            End If
        Next
salta:

        Connessione_Automatica = 0
        Stringa_di_connessione = "Impossibile connettersi al modulo in prova" & Chr(13) & "Verificare i collegamenti e l'alimentazione del lettore" & Chr(13) & "Premere un tasto"

    End Function
    Sub attendi_per(ByVal stato As String)
        Traspric = Chr("&H" + stato)
        Do While Not Traspric <> Chr("&H" + stato)
            Call traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1) = Chr(0) Then
                Traspric = Chr("&H" + stato)
            End If
            MySleep(100)

        Loop

    End Sub
    Function attendi_per_t(ByVal stato As String, ByVal lop As Long, ByVal t As Integer) As Boolean

        If t > 3000 Then PictureBox1.Visible = True

        Traspric = Chr("&H" & stato)
        Dim limite As DateTime = DateTime.Now.AddMilliseconds(t)
        Do While Not Traspric <> Chr("&H" & stato)
            If DateTime.Now > limite Then
                attendi_per_t = False
                PictureBox1.Visible = False
                Exit Function
            End If
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1000) = Chr(0) Then
                Traspric = Chr("&H" & stato)
            End If
            MySleep(lop)
            Application.DoEvents()
        Loop
        If Traspric = "" Then attendi_per_t = False : PictureBox1.Visible = False : Exit Function

        attendi_per_t = True
        PictureBox1.Visible = False
    End Function
    Function Cambio_Velocita_Seriale(ByVal velocita As Integer, ByVal invio As Boolean)
        Dim caga As String
        Traspric = ""
        'attendi_per("41")

        If invio = True Then
            If velocita = 4 Then
                Call traspsend("D37700", 0, 0)
            Else
                Call traspsend("D3470" + Trim(Str(velocita)), 0, 0)
            End If

        End If
        Call MySleep(1000)
        Select Case velocita
            Case 0
                moRS232.BaudRate = 9600

            Case 1
                moRS232.BaudRate = 19200

            Case 2
                moRS232.BaudRate = 38400

            Case 3
                moRS232.BaudRate = 57600

            Case 4
                moRS232.BaudRate = 115200
        End Select
        'If invio = True Then
        '    If velocita <> 4 Then
        '        Call ricezione("", 1, 6000)
        '    End If
        'End If
        Call MySleep(1000)
        moRS232.Close()
        moRS232.Open()
        If moRS232.InBufferCount > 0 Then
            moRS232.Read(moRS232.InBufferCount)
            caga = moRS232.InputStreamString
        End If
        caga = moRS232.BaudRate
        attendi_per_t("41", 500, 2000)

        If Traspric <> "" Then Cambio_Velocita_Seriale = True Else Cambio_Velocita_Seriale = False

    End Function
    Public Sub Ext_Trig()
        Dim aa As String
        Call traspsend("D355", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call traspsend("D35401", 1, 3000)
        End If

    End Sub
    Public Sub Int_Trig()
        Dim aa As String
        Call traspsend("D355", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call traspsend("D35400", 1, 3000)
        End If

    End Sub
    Public Sub Auto_Answ_ON()
        Dim aa As String
        Call traspsend("D353", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call traspsend("D35201", 1, 3000)
        End If
    End Sub

    Public Sub Auto_Answ_OFF()
        Dim aa As String
        Call traspsend("D353", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call traspsend("D35200", 0, 0)
            attendi_per("41")
        End If
    End Sub

    Public Sub Dbl_On()
        Dim aa As String
        Call traspsend("D349", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Call traspsend("D34801", 1, 3000)
        End If
    End Sub

    Public Sub Dbl_Off()
        Dim aa As String
        Call traspsend("D349", 1, 5)
        aa = stringa_ricezione(Traspric)
        If aa = "01" Then
            Call traspsend("D34800", 1, 3000)
        End If
    End Sub

    Public Sub Reboot(Optional ByVal cambiare_velocità As Boolean = False, Optional ByVal velocità_seriale As Integer = 0)
        'Dim usb_ric As String
        Call traspsend("D37D", 1, 5000)
        MySleep(2500)
        If cambiare_velocità = True Then
            Cambio_Velocita_Seriale(velocità_seriale, False)
        End If

        attendi_per("41")
        Call MySleep(200)
        Call traspsend("D31300", 0, 0)
        Call MySleep(200)
        Call traspsend("F3", 1, 1000)
        'For a = 1 To 3
        '    usb_ric = USBB("20", 2, 13, False)
        '    If Mid$(usb_ric, 1, 12) = "123456 prova" Then
        '        Exit Sub
        '    End If
        '    Call MySleep(2000)
        'Next

    End Sub
    Function Test_Sd_Card() As Boolean
        'Verifica dell'assenza della microSD card con la richiesta MemDump (D3 33 0…): 
        'il bit qq della locazione aaaa, indica la presenza oppure l’assenza di un supporto 
        'di memoria microSD;
        'richiedere l'inserimento della card e verificarne la presenza usando nuovamente il 
        'comando MemDump;
        'Lanciare il comando SDTest (D3 29 02) che effettua il check dell’interfaccia. 
        'In caso di fallimento verificare che la carta sia formattata e risulti funzionante.

        Dim aa As String
        Test_Sd_Card = False
        Scrivi("Verificare che non ci sia una micro SD inserita nel connettore e premere un tasto", Color.Black)
        Attendi_Tasto()
        Mem_Dump(1, "C0000000", "00000004")
        aa = stringa_ricezione(Traspric)
        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 8, 1)
        aa = hex_bin(aa)
        If Mid(aa, 4, 1) <> "0" Then
            Stringa_Errore = "Test micro SD fallito, premere un tasto per ripetere, Esc per uscire"
            Test_Sd_Card = False
            Errore_Sd_Card = "Fail | "
            Exit Function
        End If
        Scrivi("Inserire la micro SD nell'apposito connettore e premere un tasto", Color.Black)
        Attendi_Tasto()

        Mem_Dump(1, "C0000000", "00000004")
        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 8, 1)
        aa = hex_bin(aa)
        If Mid(aa, 4, 1) <> "1" Then
            Stringa_Errore = "Test micro SD fallito, premere un tasto per ripetere, Esc per uscire"
            Test_Sd_Card = False
            Errore_Sd_Card = "Fail | "
            Exit Function
        End If

        Call Traspsend("D32902", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa <> "01" Then
            Stringa_Errore = "Test micro SD fallito, premere un tasto per ripetere, Esc per uscire"
            Test_Sd_Card = False
            Errore_Sd_Card = "Fail | "
            Exit Function
        End If
        Scrivi("Test micro SD superato, rimuovere la micro SD e premere un tasto", Color.Green)
        Attendi_Tasto()
        Test_Sd_Card = False
        Mem_Dump(1, "C0000000", "00000004")
        aa = Mid(TOGLIE_0(stringa_ricezione(Traspric)), 8, 1)
        aa = hex_bin(aa)
        If Mid(aa, 4, 1) <> "0" Then
            Stringa_Errore = "Test micro SD fallito, premere un tasto per ripetere, Esc per uscire"
            Test_Sd_Card = False
            Errore_Sd_Card = "Fail | "
            Exit Function
        End If
        Test_Sd_Card = True
    End Function

    Function Test_SPI() As Boolean
        'Lo scopo del test è quello di verificare la funzionalità delle due porte SPI 
        'presenti in piastra (J6-J7). Per eseguire il test è necessario, prima di dare 
        'tensione, connettere le due seriali in loop-back attraverso l’apposito cavetto.
        'Il test è realizzato dal comando SPITest (D3 29 01) che restituisce un informazione 
        'di OK o FAULT sul funzionamento delle interfacce.
        Dim aa As String
        Test_SPI = False
        Scrivi("Invio comando SPI_Test", Color.Black)
        Call Traspsend("D32901", 1, 5000)
        aa = stringa_ricezione(Traspric)
        If aa = "00" Then
            Stringa_Errore = "Test SPI fallito, premere un tasto per ripetere, Esc per uscire"
            Test_SPI = False
            Errore_SPI = "Fail | "
            Exit Function
        End If
        Scrivi("Test SPI superato", Color.Green)
        Test_SPI = True
        Stringa_Salvataggio = Stringa_Salvataggio & "Pass |"
    End Function


    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub Debug_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeb.Click
        cmdDeb.Visible = False
        Test_Foto_Trasparenza()
    End Sub
End Class
